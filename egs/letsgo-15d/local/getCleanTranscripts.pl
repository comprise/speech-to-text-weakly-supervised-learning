#!/usr/bin/perl

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 1){
	print STDERR "USAGE: $0 <orig_annotations> <letsgo_raw_directory>\n";
	exit(0);
}

### some hard inits to choose amount of training data
my $trainYm = "200810";		
my $trainD = 15;		
my $devTestYm = "200909";

my @uttArray, @rawArray, @gtArray;
my $lastCallPath = "NULL";
my $currCallPath = "NULL";
open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	if($line =~ m/^id.+purpose$/){
		next;
	}else{
		$line =~ s/^\.\///;
		my @fields = split(/\,/, $line);
		my $rawfile = $fields[0];
		my $gt = "$fields[4]";
		
		$gt =~ s/[a-z*]\%[a-z]*/ <unk> /g;
		$gt =~ s/%+/ <unk> /g;
		my @words = split(/\s+/, $gt);
		my $newgt = "";
		foreach my $w (@words){
			if($w =~ m/^\d+[a-z]+$/){
				#print STDERR $w,"\n";
				my $wa = $w;
				$w =~ s/^\d+//;
				$wa =~ s/[a-z]+$//;
				$newgt = $newgt." $wa";	
			}elsif($w =~ m/^[a-z]+\d+$/){
				my $wa = $w;
				$wa =~ s/\d+$//;
				$w =~ s/^[a-z]+//;
				$newgt = $newgt." $wa";	
			}
			$newgt = $newgt." $w";
		}
		$gt = $newgt;
		$gt =~ s/^\s//;

		$currCallPath = $rawfile;
		$currCallPath =~ s/\/\d\d\d\.raw//;

		if(($lastCallPath eq "NULL") || ($currCallPath eq $lastCallPath)){
			push(@uttArray, $line);
			push(@rawArray, $rawfile);
			push(@gtArray, $gt);
		}else{
			my $pdir = $lastCallPath;
			$pdir =~ s/\d\d\/.+$//;
			my $lastCallId = $lastCallPath;
			$lastCallId =~ s/\//-/;
			my $logfile = $ARGV[1]."/".$pdir."/".$lastCallPath."/LetsGoPublic-".$lastCallId."-dialog.log";
			if (-e $logfile) {
				my @states = getDialogStates($logfile);
				if($#uttArray != $#states){				# inconsistency in dialog.log for this call
					#print STDERR "----$lastCallPath $#uttArray $#states-----\n";
				}else{
					for(my $i=0; $i<=$#states; $i++){
						if(($gtArray[$i] eq "NON_UNDERSTANDABLE") || ($gtArray[$i] =~ m/dtmf\_/ )){
							next;
						}
						my $rawFilePath = $pdir."/".$rawArray[$i];
						#my $size = -s $rawFilePath;
						#$size = $size / (2 * 8000); # 16bit8kHz raw file
						#print $uttArray[$i].",".$states[$i].",".$size."\n";
						my ($ym, $ymd, $c, $u) = split(/\//,$rawFilePath);
						my $m = $ym;
						$m =~ s/^\d\d\d\d//;
						$m = int($m);
						my $d = $ymd;
						$d =~ s/^\d\d\d\d\d\d//;
						$d = int($d);
						if((($ym eq $trainYm) && ($d<=$trainD)) || ($ym eq $devTestYm)){
							print $rawFilePath.",".$gtArray[$i].",".$states[$i]."\n";
						}
					}
				}
			}else{
				#print "xxx $logfile\n";	
			}

			@uttArray = ();
			@rawArray = ();
			@gtArray = ();
			push(@uttArray, $line);
			push(@rawArray, $rawfile);
			push(@gtArray, $gt);
		}

		$lastCallPath = $currCallPath;
	}
}
close($FH);


sub getDialogStates(){
	my $filename = $_[0];
	#print "===getDialogStates=== ".$filename."\n";
	my @retstates;
	my $currState = "";
	open my $FH, "<", $filename or die "can't read open '$filename': $OS_ERROR";
	while (my $line = <$FH>) {
		if($line =~ m/^dialog_state \= /){
			$currState = $line;
			$currState =~ s/^dialog_state \= //;
			$currState =~ s/\s*$//;
			#print $line;
		}elsif($line =~ m/New user input \[User\:/){
			push(@retstates,$currState);
			#print $line;
		}else{
			next;
		}
	}
	#print "===returning getDialogStates=== $filename\n";
	return @retstates;
}
