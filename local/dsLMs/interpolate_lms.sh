#!/bin/bash

# Derived software, Copyright © 2020 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# Based on Kaldi (kaldi/egs/formosa/s5/local/prepare_lm.sh)
# Copyright 2015-2016  Sarah Flora Juan
# Copyright 2016  Johns Hopkins University (Author: Yenda Trmal)
# Apache 2.0

export LC_ALL=C

echo "$0 $@"

[ -f path.sh ]  && . ./path.sh
. ./utils/parse_options.sh || exit 1

if [ $# -ne 4 ] ; then
        echo "Usage: $0 <lm> <mix-lm> <dev.txt> <out_dir>"
        exit 1;
fi

cp $3 $4

for w in 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1; do
    ngram -lm $1  -mix-lm $2 \
          -lambda $w -write-lm $4/lm.${w}.gz
    echo -n "$4/lm.${w}.gz "
    ngram -lm $4/lm.${w}.gz -ppl $3 | paste -s -
done | sort  -k15,15g  > $4/perplexities.txt

cat $4/perplexities.txt

bestLM=`head -n 1 $4/perplexities.txt | cut -f 1 -d ' '`
(cd $4; ln -sf `basename $bestLM` lm.gz )

