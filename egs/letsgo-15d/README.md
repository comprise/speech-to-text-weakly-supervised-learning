[COMPRISE](https://www.compriseh2020.eu) sample [Kaldi](http://kaldi-asr.org) recipe to train [Chain](https://kaldi-asr.org/doc/chain.html) models on the [Let's Go dataset](https://dialrc.github.io/LetsGoDataset/).

----

## Prerequisites
- This library will re-use binaries and scripts from the [Kaldi](http://kaldi-asr.org) toolkit. So you should have Kaldi pre-installed on your system. 
- It requires you to install the [kaldi_lm](http://www.danielpovey.com/files/kaldi/kaldi_lm.tar.gz) tool. You can  install this tool with the *tools/extras/install\_kaldi_lm.sh* script in your Kaldi installation.

----

## Setup
- Ensure that you have a working Kaldi installation.
- Ensure you either follow the directory structure of this repository or you have modifed *path.sh*, *cmd.sh* and other scripts in *local/* accordingly.

----

## Typical Usage Steps
Assuming you are following the directory structure of this repository.
### Prepare for training

>`bash egs/letsgo-15d/prepare_data.sh letsgo_raw_dir letsgo_transcript_2008_2009_v4.csv`

Note that [prepare_data.sh](prepare_data.sh) uses perl scripts [local/getCleanTranscripts.pl](local/getCleanTranscripts.pl) and [local/getTrainDevTestSplits.pl](local/getTrainDevTestSplits.pl) with variables fixed to decide the choice of train, dev and test set. Similarly, [local/updateLexicon.pl](local/updateLexicon.pl) script uses the pre-built [local/oovs.lexicon.txt](local/oovs.lexicon.txt).

### Train tri3 model

>`bash egs/letsgo-15d/train_tri3.sh letsgo_raw_dir`

Note that [train_tri3.sh](train_tri3.sh) uses fixed locations of *datadir*, *confdir*, *expdir*, *egslocal*

### Train chain model

>`bash egs/letsgo-15d/train_chain.sh --datadir data/letsgo-15d-seed --expdir exp/letsgo-15d-seed --localdir egs/letsgo-15d/local --confdir egs/letsgo-15d/conf`


### Decode using chain model

>`bash egs/letsgo-15d/decode_lats_with_chain.sh --datadir data/letsgo-15d-seed --expdir exp/letsgo-15d-seed --testset split/unsup`

