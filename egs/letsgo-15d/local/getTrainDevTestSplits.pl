#!/usr/bin/perl

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 1){
	print STDERR "USAGE: $0 <trascriptions.csv> <outdir>\n";
	exit(0);
}

### some hard inits to choose amount of training data
my $trainSetMonths = " 200810 ";
my $devTestMonth = "200909";	# Dev set will be calls from 1-15 of this month and Test set will be calls from 16-end of this month		

my $outdir = $ARGV[1];
$outdir =~ s/\/$//;
my $trainfile = "$outdir/train.transcriptions.csv";
my $devfile = "$outdir/dev.transcriptions.csv";
my $testfile = "$outdir/test.transcriptions.csv";
open(TRAIN, '>', $trainfile) or die $!;
open(DEV, '>', $devfile) or die $!;
open(TEST, '>', $testfile) or die $!;
open my $FH2, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH2>) {
	$line =~ s/^\.\///;
	my ($id, $txt, $ds) = split(/\,/, $line);
	my ($ym, $ymd, $c, $u) = split(/\//,$id);
	my $m = $ym;
	$m =~ s/^\d\d\d\d//;
	$m = int($m);
	my $d = $ymd;
	$d =~ s/^\d\d\d\d\d\d//;
	$d = int($d);
	#print "$ym $m $d\n";
	if($trainSetMonths =~ m/ $ym /){
		if($d<=15){
			print TRAIN $line;	
		}
	}elsif($ym eq $devTestMonth){
		if($d<=15){
			print DEV $line;	
		}else{
			print TEST $line;	
		}
	}
}
close($FH2);
close(TRAIN);
close(DEV);
close(TEST);

