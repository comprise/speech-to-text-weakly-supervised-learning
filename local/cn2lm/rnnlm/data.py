#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import torch
import torch.utils.data

import os, re, sys, numpy

from collections import Counter

numpy.random.seed(0)
torch.manual_seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

EQUI_PROB = False
BEST_SEQ = False
SKIP_EPS = True 
SKIP_NON_BEST_EPS = True
SAUS_POST_THRESH = 0.0

MAX_LEN = 15
MAX_ARCS = 5
EMB_DIM = 64
NORM_POST_OVER_ARCS = True
MINI_BATCH_SIZE = 32

ARC_PAD_SYM = '#0'
SEQ_PAD_SYM = '</s>'

class Config:
	def __init__(self, vocab_dict):
		self.vocab_dict = vocab_dict
		self.vocab_list = [None] * len(vocab_dict)
		for w in vocab_dict:
			self.vocab_list[vocab_dict[w]] = w
		self.embedding_dim = EMB_DIM
		self.arcpadid = self.vocab_dict[ARC_PAD_SYM]
		self.seqpadid = self.vocab_dict[SEQ_PAD_SYM]
		if '<UNK>' in vocab_dict:
			self.unk_sym = '<UNK>'
		else:
			self.unk_sym = '<unk>'
		self.combine_method = "best" #  "weighted-sum" #"avg"# 
		self.best_seq = BEST_SEQ

def build_config(asr_vocab_file):
	vocab_dict = {}
	last_wid = 0
	with open(asr_vocab_file) as fp:
		for line in fp:
			line = line.rstrip()
			word, wid = line.split(' ')
			vocab_dict[word] = int(wid)
			last_wid = int(wid)
	vocab_dict['<brk>'] = last_wid+1
	config = Config(vocab_dict)
	return config

def get_unsup_obs_seqs(saus_dir, config, eval=False):
	asr_vocab_list = config.vocab_list
	vocab_dict = config.vocab_dict
	obs_seq = []
	uttid_list = []
	for r, d, f in os.walk(saus_dir):
		for file in f:
			m = re.search(r'saus\-wtimes\.\d+$', file)
			if m is not None:
				_fnum = file.split('.')[-1]
				with open(os.path.join(r, file)) as fp: 
					for line in fp:
						line = line.replace('\n', ' ')

						uttid, bin_text = line.split(' ', 1)

						bin_text = bin_text.lstrip(' [')
						bin_text = bin_text.rstrip('] ')
						_bins = bin_text.split(' ] [ ')	

						seq_bin_arc_probs = []
						seq_bin_arc_ids = []
						for bin in _bins:
							if bin != '0 1':
								vals = bin.split(' ')
								vid = 0
								bin_probs = []
								arc_ids = []
								best_wrd = asr_vocab_list[int(vals[0])]
								best_scr = float(vals[1])
								if best_wrd == '<eps>' and (best_scr > 0.85 or SKIP_EPS):
									continue
								else:
									while vid < len(vals):
										wrd = asr_vocab_list[int(vals[vid])]
										vid += 1
										scr = float(vals[vid])
										vid += 1
										if EQUI_PROB:
											scr = 1.0
										if SKIP_NON_BEST_EPS:
											if (best_wrd != '<eps>' and wrd == '<eps>'):
												continue
										bin_probs.append(scr)
										arc_ids.append(vocab_dict[wrd])
										if config.best_seq:
											break
									seq_bin_arc_probs.append(bin_probs)
									seq_bin_arc_ids.append(arc_ids)

						seq_bin_arc_probs_nxt = seq_bin_arc_probs + [[1.0], [1.0]]
						seq_bin_arc_ids_nxt = seq_bin_arc_ids + [[vocab_dict['</s>']], [vocab_dict[SEQ_PAD_SYM]]]
						seq_bin_arc_probs = [[1.0]] + seq_bin_arc_probs + [[1.0]]
						seq_bin_arc_ids = [[vocab_dict['<s>']]] + seq_bin_arc_ids + [[vocab_dict['</s>']]]

						assert len(seq_bin_arc_probs) == len(seq_bin_arc_probs_nxt), "len(seq_bin_arc_probs) != len(seq_bin_arc_probs_nxt)"

						if len(seq_bin_arc_probs) <= MAX_LEN:
							obs_seq.append([seq_bin_arc_probs, seq_bin_arc_ids, seq_bin_arc_probs_nxt, seq_bin_arc_ids_nxt])
							uttid_list.append(uttid)
						else:
							begin = 0
							while begin < len(seq_bin_arc_probs):
								red_seq_probs = seq_bin_arc_probs[begin:begin+MAX_LEN]
								red_seq_ids = seq_bin_arc_ids[begin:begin+MAX_LEN]
								red_seq_probs_nxt = seq_bin_arc_probs_nxt[begin:begin+MAX_LEN]
								red_seq_ids_nxt = seq_bin_arc_ids_nxt[begin:begin+MAX_LEN]
								if len(red_seq_probs) == 0:
									break
								elif len(red_seq_probs) < MAX_LEN:
									red_seq_probs = seq_bin_arc_probs[-MAX_LEN:]
									red_seq_ids = seq_bin_arc_ids[-MAX_LEN:]
									red_seq_probs_nxt = seq_bin_arc_probs_nxt[-MAX_LEN:]
									red_seq_ids_nxt = seq_bin_arc_ids_nxt[-MAX_LEN:]
								obs_seq.append([red_seq_probs, red_seq_ids, red_seq_probs_nxt, red_seq_ids_nxt])
								begin += MAX_LEN
								uttid_list.append(uttid)
	return obs_seq, uttid_list

def get_sup_dataset(config, textfile):
	obs_seq = []
	uttid_list = []
	with open(textfile) as fp:
		for line in fp:
			line = line.rstrip()
			uttid, text = line.split(' ', 1)
			wrds = text.split()
			seq = []
			for token in wrds:
				if token == '<heps>':
					token = '<eps>'
				if token != '<eps>':	# exclude <eps>
					if token in config.vocab_dict:
						seq.append([config.vocab_dict[token]])
					else:
						# should be ignored as per srilm http://www.speech.sri.com/projects/srilm/manpages/srilm-faq.7.html but practically not so????
						seq.append([config.vocab_dict['<unk>']])
			seq_bin_arc_ids_nxt = seq + [[config.vocab_dict['</s>']], [config.vocab_dict[SEQ_PAD_SYM]]]
			seq_bin_arc_ids = [[config.vocab_dict['<s>']]] + seq + [[config.vocab_dict['</s>']]]
			seq_probs = [[1.0]] * len(seq_bin_arc_ids)

			if len(seq_bin_arc_ids) <= MAX_LEN:
				obs_seq.append([seq_probs, seq_bin_arc_ids, seq_probs, seq_bin_arc_ids_nxt])
				uttid_list.append(uttid)
			else:
				begin = 0
				while begin < len(seq_bin_arc_ids):
					red_seq_ids = seq_bin_arc_ids[begin:begin+MAX_LEN]
					red_seq_ids_nxt = seq_bin_arc_ids_nxt[begin:begin+MAX_LEN]
					red_seq_probs = [[1.0]] * len(red_seq_ids)
					if len(red_seq_ids)==0:
						break
					elif len(red_seq_ids) < MAX_LEN:
						red_seq_ids = seq[-MAX_LEN:]
					obs_seq.append([red_seq_probs, red_seq_ids, red_seq_probs, red_seq_ids_nxt])
					uttid_list.append(uttid)
					begin = begin + MAX_LEN
	return obs_seq, uttid_list

def get_saus_dataset(config, saus_dir, eval=False, sup_text=None):
	probs_n_ids, uttid_list = get_unsup_obs_seqs(saus_dir, config, eval)
	if sup_text is not None:
		sup_probs_n_ids, sup_uttid_list = get_sup_dataset(config, sup_text)
		probs_n_ids = sup_probs_n_ids + probs_n_ids
		uttid_list = sup_uttid_list + uttid_list
	shuffle_flag = True
	if eval:
		shuffle_flag=False
	dataset = SausDataset(probs_n_ids, config, shuffle_flag)
	return dataset, uttid_list

class SausDataset(torch.utils.data.Dataset):
	def __init__(self, probs_n_ids, config, shuffle_flag=True):
		self.config = config
		self.samples = probs_n_ids # list of bin seq, with word probs in each bin
		pad = Pad(self.config) # function for generating a minibatch after padding
		self.loader = torch.utils.data.DataLoader(self, batch_size=MINI_BATCH_SIZE, num_workers=1, shuffle=shuffle_flag, collate_fn=pad)

	def __len__(self):
		return len(self.samples)

	def __getitem__(self, idx):
		line = self.samples[idx]
		return line

class Pad:
	def __init__(self, config):
		self.N = len(config.vocab_dict) # num of states
		self.arcpadid = config.arcpadid
		self.seqpadid = config.seqpadid

	def __call__(self, batch):
		"""
		Returns a minibatch padded to have the same length.
		"""
		x_probs = []
		x_ids = []
		y_probs = []
		y_ids = []
		batch_size = len(batch)
		for index in range(batch_size):
			x_probs.append(batch[index][0])
			x_ids.append(batch[index][1])
			y_probs.append(batch[index][2])
			y_ids.append(batch[index][3])

		# pad all sequences to have MAX_ARCS, MAX_LEN
		x_probs_npy = numpy.zeros([batch_size, MAX_LEN, MAX_ARCS], dtype='float32')
		x_ids_npy = numpy.zeros([batch_size, MAX_LEN, MAX_ARCS], dtype='int32') + self.arcpadid
		x_mask_npy = numpy.zeros([batch_size, MAX_LEN, MAX_ARCS], dtype='float32')
		y_probs_npy = numpy.zeros([batch_size, MAX_LEN, MAX_ARCS], dtype='float32')
		y_ids_npy = numpy.zeros([batch_size, MAX_LEN, MAX_ARCS], dtype='int32') + self.arcpadid
		y_mask_npy = numpy.zeros([batch_size, MAX_LEN, MAX_ARCS], dtype='float32')
		for bid in range(batch_size):
			for tid in range(MAX_LEN):
				if tid < len(x_probs[bid]):
					num_arcs = min(MAX_ARCS, len(x_probs[bid][tid]))
					x_post_sum = 1.0
					if NORM_POST_OVER_ARCS:
						x_post_sum = sum(x_probs[bid][tid][0:num_arcs])		
					for aid in range(num_arcs):
						x_probs_npy[bid, tid, aid] = x_probs[bid][tid][aid] / x_post_sum
						x_mask_npy[bid, tid, aid] = 1.0
						x_ids_npy[bid, tid, aid] = x_ids[bid][tid][aid]
				else:
					x_probs_npy[bid, tid, 0] = 1.0
					x_mask_npy[bid, tid, 0] = 0.0
					x_ids_npy[bid, tid, 0] = self.seqpadid

			for tid in range(MAX_LEN):
				if tid < len(y_probs[bid]):
					num_arcs = min(MAX_ARCS, len(y_probs[bid][tid]))
					y_post_sum = 1.0
					if NORM_POST_OVER_ARCS:
						y_post_sum = sum(y_probs[bid][tid][0:num_arcs])		
					for aid in range(num_arcs):
						y_probs_npy[bid, tid, aid] = y_probs[bid][tid][aid] / y_post_sum
						y_mask_npy[bid, tid, aid] = 1.0
						y_ids_npy[bid, tid, aid] = y_ids[bid][tid][aid]
				else:
					y_probs_npy[bid, tid, 0] = 1.0
					y_mask_npy[bid, tid, 0] = 0.0
					y_ids_npy[bid, tid, 0] = self.seqpadid	

		# stack into single tensor
		x_probs_torch = torch.from_numpy(x_probs_npy).float()
		x_ids_torch = torch.from_numpy(x_ids_npy).long()
		x_mask_torch = torch.from_numpy(x_mask_npy).float()
		y_probs_torch = torch.from_numpy(y_probs_npy).float()
		y_ids_torch = torch.from_numpy(y_ids_npy).long()
		y_mask_torch = torch.from_numpy(y_mask_npy).float()

		return (x_probs_torch, x_ids_torch, x_mask_torch, y_probs_torch, y_ids_torch, y_mask_torch)

def get_eval_sents(config, textfile):
	vocab = config.vocab_dict
	sents = []
	num_wrds = 0
	num_sents = 0

	with open(textfile) as fp:
		for line in fp:
			seq = ['<s>']
			line = line.rstrip()
			_uttid, text = line.split(' ', 1)
			wrds = text.split()
			for token in wrds:
				if token == '<heps>':
					token = '<eps>'
				if token != '<eps>':
					if token in vocab:
						seq.append(token)
						num_wrds +=1
					else:
						# should be ignored as per srilm http://www.speech.sri.com/projects/srilm/manpages/srilm-faq.7.html but practically not so????
						seq.append(config.unk_sym) 
						num_wrds +=1
			seq.append('</s>')

			in_ids = []
			out_ids = []
			for i in range(1, len(seq)):
				in_ids.append(vocab[seq[i-1]])
				out_ids.append(vocab[seq[i]])
			sents.append([torch.tensor(in_ids), torch.tensor(out_ids)])
			
			num_sents += 1

	return sents, num_wrds, num_sents
			