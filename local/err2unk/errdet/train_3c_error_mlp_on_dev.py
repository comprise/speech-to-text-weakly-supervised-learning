#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import numpy, sys, os

from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.callbacks import EarlyStopping

from keras.callbacks import Callback
import tensorflow as tf
from keras import backend as K

from sklearn.metrics import classification_report, recall_score, precision_score, f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

import pickle

class EarlyStoppingAtMaxF1(tf.keras.callbacks.Callback):
  """Stop training when the F1 is at its max, i.e. the F1 stops increasing.

  Arguments:
      patience: Number of epochs to wait after max has been hit. After this
      number of no improvement, training stops.
  """

  def __init__(self, modelTag, patience=10):
    super(EarlyStoppingAtMaxF1, self).__init__()

    self.patience = patience

    # best_weights to store the weights at which the minimum loss occurs.
    self.best_weights = None

    self.f1_scores=[]

    self.modelTag = modelTag

  def on_train_begin(self, logs=None):
    # The number of epoch it has waited when loss is no longer minimum.
    self.wait = 0
    # The epoch the training stops at.
    self.stopped_epoch = 0
    # Initialize the best as infinity.
    self.best = -1
    self.bestEpoch = -1

  def on_epoch_end(self, epoch, logs=None):    
    y_val_pred=self.model.predict(self.validation_data[0])
    yt = []
    yp = []
    for i in range(self.validation_data[0].shape[0]):
        yt.append(numpy.argmax(self.validation_data[1][i]))
        yp.append(numpy.argmax(y_val_pred[i]))
    f1=f1_score(yt, yp, average='micro')  
    print('Epoch %d f1: %f' %(epoch, f1))
    print(classification_report(yt, yp, digits=4))
    self.f1_scores.append(f1)

    current = round(f1, 3)
    if current >= self.best:
      self.best = current
      self.wait = 0
      self.bestEpoch = epoch
      # Record the best weights if current results is better (less).
      self.best_weights = self.model.get_weights()
      self.model.save(self.modelTag) 
    else:
      self.wait += 1
      if self.wait >= self.patience:
        self.stopped_epoch = epoch
        self.model.stop_training = True
        print('Restoring model weights from the end of the best epoch: %s' % (str(self.bestEpoch)))
        self.model.set_weights(self.best_weights)

  def on_train_end(self, logs=None):
    if self.stopped_epoch > 0:
      print('Epoch %05d: early stopping' % (self.stopped_epoch + 1))

def getPredictions(X, Y, y_val_pred):
	yt = []
	yp = []
	for i in range(X.shape[0]):
		yt.append(numpy.argmax(Y[i]))
		yp.append(numpy.argmax(y_val_pred[i]))
	return yt, yp

def readFeatsForStd(featfile):
	xx = []
	yy = []
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			fvec = [float(x) for x in feats.split(' ')]
			xx.append(fvec[0:16])
			yy.append(int(label))

	return (numpy.array(xx), numpy.array(yy))

def prepFeats(featfile, scaler, numclasses):
	num_samples = 0
	tmpfeats = ''
	lastid = ''
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			tmpfeats = feats
			num_samples = num_samples + 1

	featdim = len(tmpfeats.split(' ')[0:16])

	xx = numpy.zeros([num_samples, featdim])
	yy = numpy.zeros([num_samples, numclasses])

	i=0
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			fvec = [float(x) for x in feats.split(' ')[0:16]]
			uid, unum = idd.rsplit('_', 1)
			xx[i] = scaler.transform([fvec])[0]  
			if int(label) == 1:
				yy[i,0] = 1			#no-err
			elif int(label) == 2:
				yy[i,1] = 1			#no-err eps
			elif int(label) == 3:	
				yy[i,2] = 1			#err
			i=i+1
	return (xx, yy)

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('featsfile', type=str, help='Path to file with dev set sausage feats and labels')
parser.add_argument('outdir', type=str, help='Path to output model dir')

args = parser.parse_args()

if not os.path.isfile(args.featsfile):
    raise IOError(args.featsfile)

if not os.path.isdir(args.outdir):
    raise IOError(args.outdir)

numclasses=3

(trainX, trainY) = readFeatsForStd(args.featsfile)
scaler = StandardScaler()
scaler.fit(trainX)
pickle.dump(scaler, open(args.outdir + '/scaler.pkl','wb'))

(trainX_, trainY_) = prepFeats(args.featsfile, scaler, numclasses)
indim=trainX_.shape[1]

print("Forming train-dev split from given train set...")
trainX, devX, trainY, devY = train_test_split(trainX_, trainY_, test_size=0.33, shuffle= True)

model = Sequential()
model.add(Dense(10, input_dim=indim, activation='relu'))
model.add(Dense(10, activation='relu'))
model.add(Dense(10, activation='relu'))
model.add(Dense(numclasses, activation='softmax'))

mTag = args.outdir + '/model.h5'
es=EarlyStoppingAtMaxF1(modelTag=mTag, patience=10)

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

history = model.fit(trainX, trainY, validation_data=(devX, devY), epochs=200, batch_size=32, verbose=2, callbacks=[es])

model.save(mTag)
