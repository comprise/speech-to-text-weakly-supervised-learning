#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if [ "$#" -ne 5 ]; then
        echo "Usage: $0 data_dir utt_dialog_state_csv old_lat_dir ds_lm_dir new_lat_dir"
        exit 1
fi

datadir=$1
allTransCsv=$2
oldLatdir=$3
dsLMdir=$4
newLatdir=$5

tmpdir=$newLatdir/tmp
refFile=$datadir/wav.scp

bgLMtag="infreq"

[ -f ./path.sh ] && . ./path.sh

for d in $datadir $dsLMdir $oldLatdir; do
  [ ! -d $d ] && echo "$0: no such directory $d" && exit 1;
done

for f in $refFile $allTransCsv; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

! ls $oldLatdir/lat.*.gz >/dev/null &&\
  echo "$0: No lattices in input directory $oldLatdir" && exit 1;

echo "Merging $oldLatdir/lat.*.gz into single .gz file ..."
mkdir -p $tmpdir
cat $oldLatdir/lat.*.gz > $tmpdir/lats.all-in-orig-splits.gz

rawids=$tmpdir/raw.ids
splitcsv=$tmpdir/split.csv
cat $refFile | cut -d " " -f1 | tr "_" "/" | sed s/$/.raw/ > $rawids
grep -f $rawids $allTransCsv > $splitcsv

logdir=$newLatdir/log
mkdir -p $logdir

metadir=$newLatdir/meta
mkdir -p $metadir
dslist=$metadir/dialog-states.list

cnt=0
for d in $dsLMdir/* ; do
	if [ -d ${d} ]; then
		#echo $d

		dialstate=`echo $d | rev | cut -d "/" -f1 | rev`
		if [[ $dialstate != $bgLMtag ]]; then
			cnt=$[$cnt +1]
			echo $dialstate >> $dslist
			echo "processing dialog state $dialstate"

			uttIds=`grep $dialstate'$' $splitcsv | cut -d, -f1 | tr "/" "_" | cut -d. -f1`
			echo $uttIds | tr " " "\n" > $metadir/$dialstate.uttids
			lat_rspecifier="ark:gunzip -c $tmpdir/lats.all-in-orig-splits.gz |"
			lat_wspecifier="ark:| gzip -c >$newLatdir/lat.$cnt.gz"
			lattice-copy --include=$metadir/$dialstate.uttids "$lat_rspecifier" "$lat_wspecifier" > $logdir/lattice-copy.$cnt.log 2>&1 || exit 1
		fi
	fi
done

grep -vf $dslist $splitcsv > $metadir/$bgLMtag.csv
infreqCnt=`wc -l $metadir/$bgLMtag.csv | cut -d" " -f1`
if [ "$infreqCnt" -gt 0 ]; then
	echo "processing dialog state $bgLMtag"
	cnt=$[$cnt +1]
	uttIds=`cat $metadir/$bgLMtag.csv | cut -d, -f1 | tr "/" "_" | cut -d. -f1`
	echo $uttIds | tr " " "\n" > $metadir/$bgLMtag.uttids
	lat_rspecifier="ark:gunzip -c $tmpdir/lats.all-in-orig-splits.gz |"
	lat_wspecifier="ark:| gzip -c >$newLatdir/lat.$cnt.gz"
	lattice-copy --include=$metadir/$bgLMtag.uttids "$lat_rspecifier" "$lat_wspecifier" > $logdir/lattice-copy.$cnt.log 2>&1 || exit 1
fi

echo $cnt > $newLatdir/num_jobs

