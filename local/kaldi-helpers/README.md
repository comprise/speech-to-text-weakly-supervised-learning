This directory shares some Kaldi helpers which are Kaldi .cc source code modified to achieve extended Kaldi binary functionalities.

The avaiable Kaldi helpers and steps to compile them are listed below.

**lattice-mbr-decode-wtimes**:
- Download [lattice-mbr-decode-wtimes.cc](lattice-mbr-decode-wtimes.cc) 
- `cp lattice-mbr-decode-wtimes.cc <kaldi-dir>/src/latbin/`
- Edit the '<kaldi-dir>/src/latbin/Makefile' to include 'lattice-mbr-decode-wtimes' in  variable 'BINFILES'
- `cd <kaldi-dir>/src/latbin/`
- `make`

