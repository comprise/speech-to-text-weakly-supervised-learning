#!/bin/bash

# Derived software, Copyright © 2020 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
#
# Based on Kaldi (kaldi/egs/wsj/s5/local/wsj_format_local_lms.sh), Copyright 2019 © Johns Hopkins University (author: Daniel Povey, Guoguo Chen)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

lang_suffix=
datadir=

echo "$0 $@"  # Print the command line for logging
. ./path.sh
. utils/parse_options.sh || exit 1;

lm_srcdir_3g=$datadir/local/local_lm/3gram

[ ! -d "$lm_srcdir_3g" ] && echo "No such dir $lm_srcdir_3g" && exit 1;

for d in $datadir/lang${lang_suffix}_test_tg; do
  rm -r $d 2>/dev/null
  cp -r $datadir/lang${lang_suffix} $d
done

lang=$datadir/lang${lang_suffix}

# Check a few files that we have to use.
for f in words.txt oov.int; do
  if [[ ! -f $lang/$f ]]; then
    echo "$0: no such file $lang/$f"
    exit 1;
  fi
done

# Be careful: this time we dispense with the grep -v '<s> <s>' so this might
# not work for LMs generated from all toolkits.
gunzip -c $lm_srcdir_3g/lm_unpruned.gz | \
  arpa2fst --disambig-symbol=#0 \
           --read-symbol-table=$lang/words.txt - $datadir/lang${lang_suffix}_test_tg/G.fst || exit 1;
  fstisstochastic $datadir/lang${lang_suffix}_test_tg/G.fst

exit 0;
