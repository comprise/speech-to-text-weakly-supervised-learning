#!/usr/bin/env bash

# Derived software, Copyright © 2020 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

# Based on Kaldi (kaldi/egs/wsj/s5/steps/lmrescore_const_arpa.sh)
# Copyright 2014  Guoguo Chen
# Apache 2.0

# Begin configuration section.
cmd=run.pl
skip_scoring=false
stage=1
scoring_opts=
# End configuration section.

echo "$0 $@"  # Print the command line for logging

. ./utils/parse_options.sh

if [ $# != 5 ]; then
   echo "Does language model rescoring of lattices (remove old LM, add new LM)"
   echo "Usage: $0 [options] <old-lang-dir> <new-lang-parent-dir> \\"
   echo "                   <data-dir> <input-decode-dir> <output-decode-dir>"
   echo "options: [--skip_scoring true]"
   exit 1;
fi

[ -f path.sh ] && . ./path.sh;

oldlang=$1
newlang=$2
data=$3
indir=$4
outdir=$5

dslist=$indir/meta/dialog-states.list
oldlm=$oldlang/G.fst
[ ! -f $oldlm ] && echo "$0: Missing file $oldlm" && exit 1;
[ ! -f $dslist ] && echo "$0: Missing file $dslist" && exit 1;
! ls $indir/lat.*.gz >/dev/null &&\
  echo "$0: No lattices input directory $indir" && exit 1;

if ! $skip_scoring; then
	[ ! -f $data/text ] && echo "$0: Missing file $data/text. Disable scoring if you do not have this reference transcipts." && exit 1;
fi

dslms=()
dslms+=("none")
while IFS="" read -r p || [ -n "$p" ]
do
	newlm=$newlang/$p/carpa_lm/G.carpa
	[ ! -f $newlm ] && echo "$0: Missing file $newlm" && exit 1;
	if ! cmp -s $oldlang/words.txt $newlang/$p/carpa_lm/words.txt; then
		echo "$0: $oldlang/words.txt and $newlang/$p/carpa_lm/words.txt differ: make sure you know what you are doing.";
	fi
	dslms+=("$newlm")
done < $dslist

oldlmcommand="fstproject --project_output=true $oldlm |"

mkdir -p $outdir/log
nj=`cat $indir/num_jobs` || exit 1;
nj=$[$nj -1]
cp $indir/num_jobs $outdir

if [ $stage -le 1 ]; then
	for i in $(seq 1 $nj); do
		(
			lattice-lmrescore --lm-scale=-1.0 \
			"ark:gunzip -c $indir/lat.$i.gz |" "$oldlmcommand" ark:- | \
			lattice-lmrescore-const-arpa --lm-scale=1.0 \
			ark:- "${dslms[i]}" "ark,t:|gzip -c>$outdir/lat.$i.gz" > $outdir/log/rescorelm.$i.log 2>&1 || exit 1
		) &
	done
	wait;
fi

nj=$[$nj +1] 
cp $indir/lat.$nj.gz $outdir/lat.$nj.gz 

if ! $skip_scoring && [ $stage -le 2 ]; then
  err_msg="Not scoring because local/score.sh does not exist or not executable."
  [ ! -x local/score.sh ] && echo $err_msg && exit 1;
  local/score.sh --cmd "$cmd" $scoring_opts $data $oldlang $outdir
else
  echo "Not scoring because requested so..."
fi

exit 0;