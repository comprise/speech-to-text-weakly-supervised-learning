#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if [ $# != 2 ]; then
    echo "USAGE: $0 <letsgo_raw_directory> <letsgo_transcript_2008_2009_v4.csv>"
    echo "          <letsgo_raw_directory> is the parent directory containing Let's Go raw data."
    echo "          <letsgo_transcript_2008_2009_v4.csv> are the csv annotations."
    exit
fi

rawdir=$1
annots=$2

datadir=data/letsgo-15d-seed
egslocal=egs/letsgo-15d/local

### some file checks
echo "Performing checks on $egslocal ..."
for f in $egslocal/getCleanTranscripts.pl $egslocal/getTrainDevTestSplits.pl $egslocal/lookupLexicon.pl $egslocal/updateLexicon.pl; do
  [ ! -f $f ] && echo "$0: no such file $f" && \
  echo "This could happen if you are not following the original directory structure. Check egslocal variable in this script..." && \
  exit 1;
done

echo "Performing checks on $rawdir ..."
rawcnt=`find $rawdir -type f -name "*.raw" | wc -l`
if [ $rawcnt -lt 4000 ]; then	# 4000 chosen arbitarily, just to ensure that we have the right input directory
    printf "\nError: $rawdir directory contains only $rawcnt .raw files.\n Please ensure it has enough raw files.\n\n"
    exit 0
fi

if [ -d $datadir/local ]; then
	printf "\nWarning: $datadir/local directory already exists. Backing it up to $datadir.bckp/ and creating a new $datadir/ directory!\n\n"
	cp -R $datadir/ $datadir.bckp
	mkdir $datadir/local/dict
fi

### Predefined directories
transdir="$datadir/trans/"				# directory for storing cleaned transcriptions
dictdir="$datadir/local/dict/"			# directory to store all intial lexicon related files
initLMdir="$datadir/local/local_lm"		# directory to store text for intial ARPA LM 

### Parse .trl transcriptions into a usable transcription file 
echo "Preparing transcriptions ..."
mkdir -p $transdir
perl $egslocal/getCleanTranscripts.pl $annots $rawdir > $transdir/transcriptions.csv

### Prepare train-dev-test splits
echo "Preparing train-dev-test splits ..."
perl $egslocal/getTrainDevTestSplits.pl $transdir/transcriptions.csv $transdir/

### Prepepare intial LM text
mkdir -p $initLMdir
cut -d "," -f2 $transdir/train.transcriptions.csv > $initLMdir/transcriptions.txt

# get cmudict-0.7b
echo "Preparing dictionary ..."
wget https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b -P $dictdir
if [[ "$?" != 0 ]]; then
    echo "\nError downloading https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b. Exitting!\n\n"
else
    echo "Successfully downloaded https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b !"
fi
  
### Prepare the lexicon
echo "Preparing final lexicon ..."
perl $egslocal/lookupLexicon.pl $transdir/train.transcriptions.csv $dictdir/cmudict-0.7b | sort -u > $dictdir/lexicon.txt.orig

### Prepare the data/local/dict/ directory with silence_phones.txt, 
echo "Preparing data/local/dict contents ..."
cat $dictdir/lexicon.txt.orig | cut -d " " -f2- | tr " " "\n" | sort -u | egrep "^[a-z][a-z]?$" > $dictdir/nonsilence_phones.txt
echo "sil" > $dictdir/optional_silence.txt
echo "sil" > $dictdir/silence_phones.txt
cat $dictdir/lexicon.txt.orig | cut -d " " -f2- | tr " " "\n" | sort -u | egrep "^[a-z][a-z][a-z]+" >> $dictdir/silence_phones.txt
cat $dictdir/silence_phones.txt | tr "\n" " " | sed 's/ *$/|/' | tr '|' '\n' > $dictdir/extra_questions.txt

perl $egslocal/updateLexicon.pl $dictdir/lexicon.txt.orig $egslocal/oovs.lexicon.txt > $dictdir/lexicon.txt
