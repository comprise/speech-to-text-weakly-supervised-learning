#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import torch, numpy, sys, os
from tqdm import tqdm

numpy.random.seed(0)
torch.manual_seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

from data import MINI_BATCH_SIZE, MAX_LEN, MAX_ARCS, get_eval_sents

if torch.cuda.is_available():
	GPU_FLAG = True
else:
	GPU_FLAG = False

class Trainer:
	def __init__(self, model, config, lr):
		self.model = model
		self.config = config
		self.lr = lr
		self.optimizer = torch.optim.Adam(model.parameters(), lr=self.lr)
		self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, 'min')

	def load_checkpoint(self, checkpoint_path):
		if os.path.isfile(os.path.join(checkpoint_path, "model_state.pth")):
			try:
				if self.model.is_cuda:
					self.model.load_state_dict(torch.load(os.path.join(checkpoint_path, "model_state.pth")))
				else:
					self.model.load_state_dict(torch.load(os.path.join(checkpoint_path, "model_state.pth"), map_location="cpu"))
			except Exception as e:
				print("Could not load previous model; starting from scratch")
				print(e)
		else:
			print("No previous model; starting from scratch")

	def save_checkpoint(self, epoch, checkpoint_path):
		try:
			torch.save(self.model.state_dict(), os.path.join(checkpoint_path, "model_state.pth"))
		except:
			print("Could not save model")
		
	def train(self, dataset):
		train_loss = 0
		num_samples = 0
		self.model.train()
		for _idx, batch in enumerate(tqdm(dataset.loader)):
			if batch[0].shape[0] < MINI_BATCH_SIZE:
				break
			else:
				x_probs,x_ids,x_masks,y_probs,y_ids,y_masks = batch
				batch_size = x_probs.size()[0]
				num_samples += batch_size
				ypred_probs = self.model(x_probs,x_ids,x_masks)	# [bs*maxlen,vocab_size]
				loss = self.model.compute_loss(ypred_probs, y_probs, y_ids, y_masks)
				self.optimizer.zero_grad()
				loss.backward()
				self.optimizer.step()
				train_loss += loss.cpu().data.numpy().item() * batch_size
		train_loss /= num_samples
		return train_loss

	def test(self, dataset, print_interval=20):
		test_loss = 0
		num_samples = 0
		self.model.eval()
		for _idx, batch in enumerate(dataset.loader):
			if batch[0].shape[0] < MINI_BATCH_SIZE:
				break
			else:
				x_probs,x_ids,x_masks,y_probs,y_ids,y_masks = batch
				batch_size = x_probs.size()[0]
				num_samples += batch_size
				ypred_probs = self.model(x_probs,x_ids,x_masks)	# [bs*maxlen,vocab_size]
				loss = self.model.compute_loss(ypred_probs, y_probs, y_ids, y_masks)
				test_loss += loss.cpu().data.numpy().item() * batch_size
		test_loss /= num_samples
		self.scheduler.step(test_loss) # if the validation loss hasn't decreased, lower the learning rate
		return test_loss

	def calc_ppl(self, sents, NW, NS):
		set_logp = 0.0
		self.model.eval()
		for in_ids, out_ids in sents:
			sent_logp = self.model.sent_logp(in_ids, out_ids)
			set_logp += sent_logp.cpu().data.numpy().item()
		logp = set_logp / numpy.log(10) # logn to log10
		ppl = 10.0**(-1 * logp / (NW + NS))
		ppl1 = 10.0**(-1 * logp / NW)	
		print("logp: %.2f | NW: %d | NS: %d | ppl: %.4f | ppl1: %.4f" % (logp, NW, NS, ppl, ppl1) )
		return ppl

	def setup_ext_eval(self, config, dev_text):
		self.ext_eval_dataset = get_eval_sents(config, dev_text)
		print('Done setting up extrinsic evaluation data ...')

	def ext_eval(self):
		sents, NW, NS = self.ext_eval_dataset
		ppl = self.calc_ppl(sents, NW, NS)
		return ppl
