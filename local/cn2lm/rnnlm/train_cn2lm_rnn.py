#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import torch
from models import SausRNN
from data import get_saus_dataset, build_config
from training import Trainer

import argparse, os, sys

torch.manual_seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

parser = argparse.ArgumentParser()
parser.add_argument('asr_vocab_file', type=str, help='Path to ASR words.txt file')
parser.add_argument('sup_text', type=str, help='Path to sup text file')
parser.add_argument('unsup_saus_dir', type=str, help='Path to unsup saus dir')
parser.add_argument('dev_saus_dir', type=str, help='Path to dev saus dir')
parser.add_argument('dev_text', type=str, help='Path to dev text file')
parser.add_argument('out_dir', type=str, help='output dir')
args = parser.parse_args()

if not os.path.isfile(args.asr_vocab_file):
	raise IOError(args.asr_vocab_file)

if not os.path.isfile(args.sup_text):
	raise IOError(args.sup_text)

if not os.path.isdir(args.unsup_saus_dir):
	raise IOError(args.unsup_saus_dir)

if not os.path.isdir(args.dev_saus_dir):
	raise IOError(args.dev_saus_dir)

if not os.path.isfile(args.dev_text):
	raise IOError(args.dev_text)

if not os.path.isdir(args.out_dir):
	raise IOError(args.out_dir)

print('Preparing datasets ...')
config = build_config(args.asr_vocab_file)
train_dataset, _ = get_saus_dataset(config, args.unsup_saus_dir, eval=False, sup_text=args.sup_text)
valid_dataset, _ = get_saus_dataset(config, args.dev_saus_dir, eval=True, sup_text=None)
checkpoint_path = args.out_dir

# Initialize and train the model
model = SausRNN(config)
num_epochs = 100
trainer = Trainer(model, config, lr=0.001)
trainer.setup_ext_eval(config, args.dev_text)
trainer.load_checkpoint(checkpoint_path)

pplist = [10000000]
for epoch in range(num_epochs):
	print("========= Epoch %d of %d =========" % (epoch+1, num_epochs))
	train_loss = trainer.train(train_dataset)
	valid_loss = trainer.test(valid_dataset)
	ppl = trainer.ext_eval()
	if ppl <= min(pplist):
		trainer.save_checkpoint(epoch, checkpoint_path)
	pplist.append(ppl)
	print("========= Results: epoch %d of %d =========" % (epoch+1, num_epochs))
	print("Epoch %d |train::| loss: %.2f |valid::| loss: %.2f | ppl: %.2f" % (epoch+1, train_loss, valid_loss, ppl))
