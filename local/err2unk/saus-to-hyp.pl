#!/usr/bin/perl

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV !=0){
	print STDERR "\nUSAGE: $0 <saus_dir>\n\n";
	exit;
}

opendir my $dh, $ARGV[0];
while (my $entry = readdir $dh) {
	my $fullname = "$ARGV[0]/$entry";
	if($fullname =~ m/\/saus-wtimes.\d+$/){
		open my $FH, "<", $fullname or die "can't read open '$fullname': $OS_ERROR";
		while (my $line = <$FH>) {
			my ($uttid, $binText) = split(/\s+/, $line, 2);
			$binText =~ s/\s*\[ 0 1 \]\s*/ /g;			# remove all epsilon only tokens from ASR
			$binText =~ s/^\s*\[\s*//;
			$binText =~ s/\s*\]\s*$//;
			my $hyp = $uttid;
			my @bins = split(/ \] \[ /, $binText);
			foreach my $bin (@bins){
				my @arcs = split(/\s+/, $bin);
				$hyp .= " ".$arcs[0]
			}
			print $hyp."\n";
		}
		close($FH);
	}
}
