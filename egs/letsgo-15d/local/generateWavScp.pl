#!/usr/bin/perl

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 2){
	print STDERR "USAGE: $0 <signal_parent_directory> <trascriptions.csv> <out_dir>\n";
	exit(0);
}

use File::Spec;

my %signalPath;
my @dirs = (File::Spec->rel2abs($ARGV[0]));
while (@dirs) {
	my $thisdir = shift @dirs;
	opendir my $dh, $thisdir;
	while (my $entry = readdir $dh) {
		next if $entry eq '.';
		next if $entry eq '..';
		
		my $fullname = "$thisdir/$entry";
				
		if (-d $fullname ){
			push @dirs, $fullname;
		}elsif($fullname =~ m/\.raw$/){		
			if($fullname =~ m/\d+\/\d\d\d\.raw$/){
				my @tmp = split(/\//,$fullname);
				my $fname = $tmp[$#tmp-3]."/".$tmp[$#tmp-2]."/".$tmp[$#tmp-1]."/".$tmp[$#tmp];
				$signalPath{$fname} = $fullname;
				#print "$fname  ",$fullname,"\n";
			}
		}
	}
}

my $outdir = $ARGV[2];
$outdir =~ s/\/$//;
my %spk2utt;
my $txtfile = "$outdir/text.unsorted";
my $wavscpfile = "$outdir/wav.scp.unsorted";
my $utt2spkfile = "$outdir/utt2spk.unsorted";
open(TEXTF, '>', $txtfile) or die $!;
open(WAVSCP, '>', $wavscpfile) or die $!;
open(UTT2SPK, '>', $utt2spkfile) or die $!;
open my $FH, "<", $ARGV[1] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	my ($id, $txt, $ds) = split(/\,/, $line);
	my $newid = $id;
	$newid =~ s/\//_/g;
	my $spkrid = $newid;
	$spkrid =~ s/\_\d\d\d\.raw$//;
	$newid =~ s/\.raw$//;
	if(exists $signalPath{$id}){
		if(length($txt)<=0){
			print TEXTF $newid," <unk>\n";
		}else{
			print TEXTF $newid," ",$txt."\n";
		}
		print WAVSCP $newid," sox -r 8000 -e signed-integer -b 16 -c 1 ",$signalPath{$id}," -t wav - | \n";
		print UTT2SPK $newid," ",$spkrid,"\n";
		if(exists $spk2utt{$spkrid}){
			$spk2utt{$spkrid} = "$spk2utt{$spkrid} $newid";
		}else{
			$spk2utt{$spkrid} = "$newid";
		}
	}else{
		print "Warning: Missing wav file corresponding to $id\n";
	}
}
close(TEXTF);
close(UTT2SPK);
close(WAVSCP);
close($FH);

my $spk2uttfile = "$outdir/spk2utt.unsorted";
open(SPK2UTT, '>', $spk2uttfile) or die $!;
foreach my $spkr (keys %spk2utt){
	my $list = $spk2utt{$spkr};
	$list =~ s/^\s+//;
	my @tmp = split(/\s+/, $list);
	my @sorted = sort @tmp;
	my $utts = join(" ", @sorted);
	print SPK2UTT "$spkr $utts\n";
}
close(SPK2UTT);

exit(0);


