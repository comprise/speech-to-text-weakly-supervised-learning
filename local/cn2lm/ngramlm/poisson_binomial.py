#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import numpy

PK_EPS_VALUE = 0.01
PK_UPPER_LIMIT = 0.99
MAX_R = 7

class PB:
    def __init__(self, pks):
        self.pks = pks
        self.number_trials = len(pks)
        self.s_k_r = self.poibin_recursive(pks)

    def pmf(self, l):
        return self.s_k_r[l]

    def poibin_recursive(self, pks):
        s_k_r = numpy.zeros([len(pks)+1, MAX_R])
        for k in range(len(pks)+1):
            for r in range(MAX_R):
                if k==0 and r==0:
                    s_k_r[k,r] = 1.0
                else:
                    _k = k-1
                    _r = r-1
                    partA = s_k_r[_k,r] * (1-pks[_k])
                    if _r < 0 or s_k_r[_k,_r] < PK_EPS_VALUE:
                        partB = 0
                    else:
                        partB = s_k_r[_k,_r] * pks[_k]
                    s_k_r[k,r] = partA + partB
        return s_k_r[-1,:]