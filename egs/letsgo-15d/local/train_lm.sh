#!/bin/bash

# Derived software, Copyright © 2020 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
#
# Based on Kaldi (kaldi/egs/wsj/s5/local/wsj_format_local_lms.sh), Copyright 2019 © Johns Hopkins University (author: Daniel Povey, Guoguo Chen)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING


datadir=$1 #data/letsgo-15d-seed/

if [ ! -d $datadir/local ]; then
  echo "$0: no input directory $datadir/local" && \
  echo "This could happen if you are not following the original directory structure. Check egslocal variable in this script..." && \
  exit 1;
fi

dir=$datadir/local/local_lm
srclex=$datadir/local/dict/lexicon.txt

if [ ! -f $dir/transcriptions.txt -o ! -f $srclex ]; then
  echo "Expecting files $dir/transcriptions.txt and $srclex to exist";
  exit 1;
fi

# Get a wordlist-- keep everything but silence, which should not appear in
# the LM.
awk '{print $1}' $srclex | grep -v -w '!SIL' > $dir/wordlist.txt

# Get training data with OOV words (w.r.t. our current vocab) replaced with <unk>.
echo "Getting training data with OOV words replaced with <unk> (train_nounk.gz)" 
cat $dir/transcriptions.txt | awk -v w=$dir/wordlist.txt \
  'BEGIN{while((getline<w)>0) v[$1]=1;}
  {for (i=1;i<=NF;i++) if ($i in v) printf $i" ";else printf "<unk> ";print ""}'|sed 's/ $//g' \
  | gzip -c > $dir/train_nounk.gz

# Get unigram counts (without bos/eos, but this doens't matter here, it's
# only to get the word-map, which treats them specially & doesn't need their
# counts).
# Add a 1-count for each word in word-list by including that in the data,
# so all words appear.
gunzip -c $dir/train_nounk.gz | cat - $dir/wordlist.txt | \
  awk '{ for(x=1;x<=NF;x++) count[$x]++; } END{for(w in count){print count[w], w;}}' | \
 sort -nr > $dir/unigram.counts

# Get "mapped" words-- a character encoding of the words that makes the common words very short.
cat $dir/unigram.counts  | awk '{print $2}' | get_word_map.pl "<s>" "</s>" "<unk>" > $dir/word_map

gunzip -c $dir/train_nounk.gz | awk -v wmap=$dir/word_map 'BEGIN{while((getline<wmap)>0)map[$1]=$2;}
  { for(n=1;n<=NF;n++) { printf map[$n]; if(n<NF){ printf " "; } else { print ""; }}}' | gzip -c >$dir/train.gz

# To save disk space, remove the un-mapped training data.  We could
# easily generate it again if needed.
rm $dir/train_nounk.gz 

#train_lm.sh --arpa --lmtype 3gram $dir
train_lm_small-data.sh --arpa --lmtype 3gram $dir		# this step and train_lm_small-data.sh is to handle small datatsets

exit 0;
