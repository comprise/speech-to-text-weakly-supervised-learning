#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if [ "$#" -ne 3 ]; then
        echo "Usage: $0 old_lang_test_dir utt_dialog_state_csv ds_lm_dir"
        exit 1
fi

langdir=$1
traincsv=$2
dsLMdir=$3

minDsCnt=100
unksym="<unk>"

wordlist=$langdir/words.txt
tmpdir=$dsLMdir/tmp

for d in $langdir; do
  [ ! -d $d ] && echo "$0: no such directory $d" && exit 1;
done

for f in $wordlist $traincsv $langdir/G.fst; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

mkdir -p $tmpdir
mkdir -p $dsLMdir

dsStats=$dsLMdir/ds-stats
cat $traincsv | cut -d, -f3 | rev | cut -d "/" -f1 | rev | sort | uniq -c | sort -nrk1 | sed 's/^[ \t]*//' > $dsStats

while IFS="" read -r p || [ -n "$p" ]
do
	#printf '%s\n' "$p"
	cnt=$(echo $p | cut -f1 -d' ')
	ds=$(echo $p | cut -f2 -d' ')
	if [ "$cnt" -gt "$minDsCnt" ]
	then
		echo "Creating LM for dialog state $ds ..."
		lmdir=$dsLMdir/$ds
		mkdir $lmdir
		grep $ds $traincsv | cut -d, -f2 > $lmdir/dsTxt									# 200810/20081001/000/001.raw,59 u,/LetsGoPublic/PerformTask/GetQuerySpecs/AskHowMayIHelpYou
		grep $ds $traincsv | cut -d, -f1 | tr "/" "_" | cut -d. -f1 > $lmdir/uttIds		# 200810/20081001/000/001.raw --> 200810_20081001_000_001

		bash local/dsLMs/train_lms_srilm.sh --words-file $wordlist --train-text $lmdir/dsTxt --oov-symbol $unksym $tmpdir $lmdir/srilm > $lmdir/srilm-train.log 2>&1 || exit 1 # Note: $tmpdir is place holder and not used by the script with these args
		bash utils/build_const_arpa_lm.sh $lmdir/srilm/lm.gz $langdir $lmdir/carpa_lm > $lmdir/carpa-build.log 2>&1 || exit 1
	else
		echo "Skipping LM for dialog state $ds with $cnt utts. Orig LM should be used instead ..."
		lmdir=$dsLMdir/infreq
	    uttIds=`grep $ds $traincsv | cut -d, -f1 | tr "/" "_" | cut -d. -f1`
		OUT=$lmdir/uttIds
		if [ ! -f "$OUT" ]; then
			mkdir -p "`dirname \"$OUT\"`" 2>/dev/null
		fi
		echo $uttIds | tr " " "\n" >> $OUT

	fi     
done < $dsStats

rm -R $tmpdir
