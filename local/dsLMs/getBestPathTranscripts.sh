#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if [ "$#" -ne 5 ]; then
        echo "Usage: $0 rescored_lat_dir words_file lm_wt word_ins_penalty unsup_text"
        exit 1
fi

dir=$1
symtab=$2
lmwt=$3
wip=$4
out=$5

[ -f path.sh ] && . ./path.sh;

for f in $symtab $dir/lat.1.gz; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

if ! [[ "$lmwt" =~ ^[0-9]+$ ]]
    then
        echo "lm_wt should be an integer between 7 and 17, which gives the best dev set WER."  && exit 1;
fi

 lattice-scale --inv-acoustic-scale=$lmwt "ark:gunzip -c $dir/lat.*.gz|" ark:- | \
    lattice-add-penalty --word-ins-penalty=$wip ark:- ark:- | \
    lattice-best-path --word-symbol-table=$symtab ark:- ark,t:- 2> /dev/null | \
    utils/int2sym.pl -f 2- $symtab > $out || exit 1;

