#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import torch, numpy, os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from data import build_config
from models import SausRNN

import argparse, os, sys

torch.manual_seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

parser = argparse.ArgumentParser()
parser.add_argument('asr_vocab_file', type=str, help='Path to ASR words.txt')
parser.add_argument('kaldi_gru_lm_template_file', type=str, help='Path to Kaldi GRU LM template model text')
parser.add_argument('pytorch_model', type=str, help='Path to Pytorch GRU LM file')
parser.add_argument('out_kaldi_model_dir', type=str, help='output Kaldi GRU LM dir')
args = parser.parse_args()

if not os.path.isfile(args.asr_vocab_file):
	raise IOError(args.asr_vocab_file)

if not os.path.isfile(args.kaldi_gru_lm_template_file):
	raise IOError(args.kaldi_gru_lm_template_file)

if not os.path.isfile(args.pytorch_model):
	raise IOError(args.pytorch_model)

if not os.path.isdir(args.out_kaldi_model_dir):
	raise IOError(args.out_kaldi_model_dir)

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

# Initialize model
config = build_config(args.asr_vocab_file)
model = SausRNN(config)
model.load_checkpoint(args.pytorch_model)

for param_tensor in model.state_dict():
    print(param_tensor, "\t", model.state_dict()[param_tensor].size())    

emb = model.state_dict()['embedding.weight'].cpu().data.numpy()
weight_ih_l0 = model.state_dict()['rnn.weight_ih_l0'].cpu().data.numpy()
w_ir, w_iz, w_in = numpy.split(weight_ih_l0, 3, axis=0)
weight_hh_l0 = model.state_dict()['rnn.weight_hh_l0'].cpu().data.numpy()
w_hr, w_hz, w_hn = numpy.split(weight_hh_l0, 3, axis=0)

bias_ih_l0 = model.state_dict()['rnn.bias_ih_l0'].cpu().data.numpy()
b_ir, b_iz, b_in = numpy.split(bias_ih_l0, 3, axis=0)
bias_hh_l0 = model.state_dict()['rnn.bias_hh_l0'].cpu().data.numpy()
b_hr, b_hz, b_hn = numpy.split(bias_hh_l0, 3, axis=0)

def skip_lines_n_print_params(wt, b, in_fp, out_fp):
    for i in range(wt.shape[0]):
        for n in wt[i]:
            print(" %f" % n, end='', file=out_fp)
        if i==wt.shape[0]-1:
            print(" ]", file=out_fp)
        else:
            print("", file=out_fp)
    print("<BiasParams>  [ ", end='', file=out_fp)
    for n in b:
        print(" %f" % n, end='', file=out_fp)
    print(" ]", file=out_fp)

def print_sig_lines(in_fp, out_fp, tanh=False):
    print('<DerivAvg>  [ ]', file=out_fp)
    print('<Count> 0 <OderivRms>  [ ]', file=out_fp)
    if tanh:
        print('<OderivCount> 0 <NumDimsSelfRepaired> 0 <NumDimsProcessed> 0 <SelfRepairScale> 1e-05 </TanhComponent>', file=out_fp)
    else:
        print('<OderivCount> 0 <NumDimsSelfRepaired> 0 <NumDimsProcessed> 0 <SelfRepairScale> 1e-05 </SigmoidComponent>', file=out_fp)

cur_component = ''
out_fp = open(os.path.join(args.out_kaldi_model_dir, "final"), 'w')
with open(args.kaldi_gru_lm_template_file) as in_fp:
    line = in_fp.readline()
    while line:
        if '<ComponentName> gru1.W_ir' in line:
            print(line, end='', file=out_fp)
            skip_lines_n_print_params(w_ir, b_ir, in_fp, out_fp)
        elif '<ComponentName> gru1.W_hr' in line:
            print(line, end='', file=out_fp)
            skip_lines_n_print_params(w_hr, b_hr, in_fp, out_fp)
        elif '<ComponentName> gru1.W_iz' in line:
            print(line, end='', file=out_fp)
            skip_lines_n_print_params(w_iz, b_iz, in_fp, out_fp)
        elif '<ComponentName> gru1.W_hz' in line:
            print(line, end='', file=out_fp)
            skip_lines_n_print_params(w_hz, b_hz, in_fp, out_fp)
        elif '<ComponentName> gru1.W_in' in line:
            print(line, end='', file=out_fp)
            skip_lines_n_print_params(w_in, b_in, in_fp, out_fp)
        elif '<ComponentName> gru1.W_hn' in line:
            print(line, end='', file=out_fp)
            skip_lines_n_print_params(w_hn, b_hn, in_fp, out_fp)
        elif '<ComponentName> gru1.Sig_r <SigmoidComponent>' in line:
            print('<ComponentName> gru1.Sig_r <SigmoidComponent> <Dim> 64 <ValueAvg>  [ ]', file=out_fp)
            print_sig_lines(in_fp, out_fp)
        elif '<ComponentName> gru1.Sig_z <SigmoidComponent>' in line:
            print('<ComponentName> gru1.Sig_z <SigmoidComponent> <Dim> 64 <ValueAvg>  [ ]', file=out_fp)
            print_sig_lines(in_fp, out_fp)
        elif '<ComponentName> gru1.Tan_n <TanhComponent>' in line:
            print('<ComponentName> gru1.Tan_n <TanhComponent> <Dim> 64 <ValueAvg>  [ ]', file=out_fp)
            print_sig_lines(in_fp, out_fp, tanh=True)
        else:
            print(line, end='', file=out_fp)
        line = in_fp.readline()
out_fp.close()

out_fp = open(os.path.join(args.out_kaldi_model_dir, "word_embedding.final"), 'w')
print("[", file=out_fp)
for i in range(emb.shape[0]):
    for n in emb[i]:
        print(" %f" % n, end='', file=out_fp)
    if i==emb.shape[0]-1:
        print(" ]", file=out_fp)
    else:
        print("", file=out_fp)
