#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

echo ""
echo "$0 $@"  # Print the command line for logging
echo 

. ./path.sh  # set paths to binaries

if [ $# != 4 ]; then
	echo ""
	echo "USAGE: $0 unsup_text old_sup_data_dir old_unsup_data_dir new_data_dir"
	echo ""
	exit 1;
fi

unsuptxt=$1
supdir=$2
unsupdir=$3
newdir=$4

for f in $unsuptxt $supdir/wav.scp $supdir/text $supdir/utt2spk $supdir/spk2utt $unsupdir/wav.scp $unsupdir/utt2spk; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

mkdir -p $newdir/tmp

grep -Pv "\d+ (\<unk\>\s*)+$" $unsuptxt | cut -d' ' -f1 > $newdir/tmp/ids  # skip all <unk> utts 
grep -f $newdir/tmp/ids $unsuptxt > $newdir/tmp/text
grep -f $newdir/tmp/ids $unsupdir/wav.scp > $newdir/tmp/wav.scp
grep -f $newdir/tmp/ids $unsupdir/utt2spk > $newdir/tmp/utt2spk
utils/utt2spk_to_spk2utt.pl < $newdir/tmp/utt2spk > $newdir/tmp/spk2utt

echo "Combining supervised and unsupervised data into $newdir ..."
utils/combine_data.sh $newdir/foo $supdir $newdir/tmp > $newdir/combine.log || exit 1
mv $newdir/foo/* $newdir
rm -R $newdir/foo/
rm -f $newdir/feats.scp $newdir/cmvn.scp $newdir/frame_shift
echo "Completed successfully!"


