#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import torch, numpy, os, sys

from data import MINI_BATCH_SIZE, MAX_LEN, MAX_ARCS

numpy.random.seed(0)
torch.manual_seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

USE_GRU = True	 # needs to be True to use current support for Kaldi, can be false otherwise
SHARE_EMB = True # needs to be True to use current support for Kaldi, can be false otherwise

class SausRNN(torch.nn.Module):
	def __init__(self, config):
		super(SausRNN, self).__init__()
		self.vocab_size = len(config.vocab_dict)
		self.embedding_dim = config.embedding_dim
		self.combine_method = config.combine_method
		if self.combine_method not in ["weighted-sum", "sum", "best", "avg"]:
			raise ValueError(f"combine_method '{self.combine_method}' not supported")

		if SHARE_EMB:
			emb_w = torch.Tensor(self.vocab_size, self.embedding_dim)
			stdv = 1. / numpy.sqrt(emb_w.size(1)) # like in nn.Linear
			emb_w.uniform_(-stdv, stdv)
			self.embedding = torch.nn.Embedding.from_pretrained(emb_w, freeze=False, padding_idx=config.arcpadid)
		else:
			self.embedding = torch.nn.Embedding(num_embeddings = self.vocab_size, embedding_dim = self.embedding_dim, padding_idx=config.arcpadid)

		if USE_GRU:
			self.rnn = torch.nn.GRU(
				self.embedding_dim,
				self.embedding_dim,
				num_layers=1,
				bias=True,
				batch_first=True,
				bidirectional=False
			)
		else:
			self.rnn = torch.nn.LSTM(
				self.embedding_dim,
				self.embedding_dim,
				num_layers=1,
				batch_first=True,
				bidirectional=False
			)

		if SHARE_EMB:
			self.hidden_to_target = torch.nn.Linear(self.embedding_dim, self.vocab_size, bias=False)
			#self.hidden_to_target.weight.data = self.embedding.weight.data # creates problem!!! https://discuss.pytorch.org/t/how-to-use-shared-weights-in-different-layers-of-a-model/71263/2
			del self.hidden_to_target.weight		
		else:
			self.hidden_to_target = torch.nn.Linear(self.embedding_dim, self.vocab_size)

		self.target_probs = torch.zeros((MINI_BATCH_SIZE * MAX_LEN, self.vocab_size))
		self.loss_mask = torch.zeros(MINI_BATCH_SIZE * MAX_LEN)
		self.KLLoss = torch.nn.KLDivLoss(reduction = 'none')

		self.is_cuda = torch.cuda.is_available()
		if self.is_cuda: 
			self.cuda()
			self.target_probs = self.target_probs.cuda()
			self.loss_mask = self.loss_mask.cuda()

	def forward(self, x_probs, x_ids, x_masks):
		if self.is_cuda:
			x_probs = x_probs.cuda()
			x_ids = x_ids.cuda()
			x_masks = x_masks.cuda()
		device = x_probs.device

		bs, maxlen, maxarcs = x_probs.size()

		h = torch.zeros((1, bs, self.rnn.hidden_size)).to(device)
		if not USE_GRU:
			c = torch.zeros((1, bs, self.rnn.hidden_size)).to(device)
		t_hs = []
		for t in range(maxlen):
			inemb = self.embedding(x_ids[:,t,:].flatten()).view(bs * maxarcs, 1, self.embedding_dim)
			h_repeated = h.repeat(1,1,maxarcs).view(1,bs*maxarcs,self.embedding_dim)
			if not USE_GRU:
				c_repeated = c.repeat(1,1,maxarcs).view(1,bs*maxarcs,self.embedding_dim)
				_output, (hs, cs) = self.rnn(inemb, (h_repeated, c_repeated)) # output -> [bs * maxarcs, t=1, 1*self.embedding_dim] , h,c -> [1*1, bs * maxarcs, self.embedding_dim]
				hs = hs.view(bs,maxarcs,self.embedding_dim)
				cs = cs.view(bs,maxarcs,self.embedding_dim)
				h, c = self.get_pooled_states(t, x_probs, hs, cs) # [bs,maxarcs,embedding_dim] -> [bs,embedding_dim]
			else:
				_output, hs = self.rnn(inemb, h_repeated) # output -> [bs * maxarcs, t=1, 1*self.embedding_dim] , h,c -> [1*1, bs * maxarcs, self.embedding_dim]
				hs = hs.view(bs,maxarcs,self.embedding_dim)
				h, _ = self.get_pooled_states(t, x_probs, hs) # [bs,maxarcs,embedding_dim] -> [bs,embedding_dim]
			t_hs.append(h.view(bs,self.embedding_dim)) 

		t_hs = torch.stack(t_hs,dim=1) # list([bs,embedding_dim]) * t -> [bs,t,embedding_dim]
		lstm_outputs = t_hs.view(bs*maxlen, self.embedding_dim)
		if SHARE_EMB:
			self.hidden_to_target.weight = self.embedding.weight
		linear_out = self.hidden_to_target(lstm_outputs) # [bs*t,embedding_dim] -> [bs*t,vocab_size]
		log_probs = torch.nn.functional.log_softmax(linear_out,dim=1) # [bs*t,vocab_size]

		return log_probs

	def get_pooled_states(self, t, x_probs, hs, cs=None):
		bs, _maxlen, maxarcs = x_probs.size()
		cp = None
		if self.combine_method == "best":
			hp = hs[:,0,:]
			if cs is not None:
				cp = cs[:,0,:]
		elif self.combine_method == "sum":
			hp = hs.sum(dim=1)
			if cs is not None:
				cp = cs.sum(dim=1)
		elif self.combine_method == "avg":
			wts = x_probs[:,t,:].view(bs,maxarcs)
			ones = torch.ones_like(wts)
			wts = torch.where(wts>0,ones,wts)
			hp = hs * wts.view(bs,maxarcs,1)
			hp = hs.sum(dim=1) / wts.sum(1)[:,None]
			if cs is not None:
				cp = cs * wts.view(bs,maxarcs,1)		
				cp = cs.sum(dim=1)/ wts.sum(1)[:,None]			
		else: # "weighted-sum"
			hp = hs * x_probs[:,t,:].view(bs,maxarcs,1)
			hp = hp.sum(dim=1)
			if cs is not None:
				cp = cs * x_probs[:,t,:].view(bs,maxarcs,1)
				cp = cp.sum(dim=1)
		return hp,cp

	def compute_loss(self, y_pred_probs, y_probs, y_ids, y_masks):
		self.target_probs.zero_() 
		self.loss_mask.zero_()
		device = y_pred_probs.device

		log_prob_indices = torch.arange(y_pred_probs.shape[0]).view(-1,1).to(device)
		vocab_indices = y_ids.view(MINI_BATCH_SIZE * MAX_LEN, MAX_ARCS).to(device)
		vocab_probs = y_probs.view(MINI_BATCH_SIZE * MAX_LEN, MAX_ARCS).to(device)
		self.target_probs[log_prob_indices, vocab_indices]=vocab_probs # note padded arcs will have zero probs, so taken care
		self.loss_mask = y_masks.sum(dim=2).flatten().to(device)
		self.loss_mask[torch.where(self.loss_mask>0)] = 1.0
		loss = self.KLLoss(y_pred_probs, self.target_probs) * self.loss_mask[:,None]	# [bs*maxlen,vocab_size]
		loss = loss.sum() / self.loss_mask.sum()
		
		return loss

	def sent_logp(self, in_ids, out_ids):
		if self.is_cuda:
			in_ids = in_ids.cuda()
			out_ids = out_ids.cuda()
		device = in_ids.device

		bs = 1
		maxlen = len(in_ids)
		inemb = self.embedding(in_ids.view(bs,maxlen))
		h = torch.zeros((1, bs, self.rnn.hidden_size)).to(device)
		if not USE_GRU:
			c = torch.zeros((1, bs, self.rnn.hidden_size)).to(device)
			output, (h, c) = self.rnn(inemb, (h, c))  # output -> [bs=1, len, 1*self.embedding_dim]
		else:
			output, h = self.rnn(inemb, h)  # output -> [bs=1, len, 1*self.embedding_dim]
		if SHARE_EMB:
			self.hidden_to_target.weight = self.embedding.weight
		linear_out = self.hidden_to_target(output.view(maxlen,self.rnn.hidden_size)) # [bs*t,embedding_dim] -> [bs*t,vocab_size]
		log_probs = torch.nn.functional.log_softmax(linear_out,dim=1) # [bs*t,vocab_size]
		pred_logp = torch.gather(log_probs, 1, out_ids.view(-1,1)).sum()

		return pred_logp

	def load_checkpoint(self, checkpoint_path):
		self.hidden_to_target = torch.nn.Linear(self.embedding_dim, self.vocab_size, bias=False)
		self.hidden_to_target.weight.data = self.embedding.weight.data
		if os.path.isfile(os.path.join(checkpoint_path)):
			try:
				if self.is_cuda:
					self.load_state_dict(torch.load(checkpoint_path))
				else:
					self.load_state_dict(torch.load(checkpoint_path, map_location="cpu"))
			except Exception as e:
				print("Could not load previous model; Exitting")
				print(e)
				sys.exit()
		else:
			print("No previous model; Exitting")
			sys.exit()
