#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if [ "$#" -ne 4 ]; then
        echo "Usage: $0 old_lang_test_dir old_lm_arpa ds_lm_dir int_ds_lm_dir"
        exit 1
fi

langdir=$1
baseLM=$2
dsLMdir=$3
dsIntLMdir=$4

bgLMtag="infreq"

for d in $dsLMdir $langdir; do
	[ ! -d $d ] && echo "$0: no such directory $d" && exit 1;
done

if [ ! -f $baseLM ]; then
	echo "$0: no such file $baseLM " && exit 1;
fi

for d in $dsLMdir/* ; do
	if [ -d ${d} ]; then
		dialstate=`echo $d | rev | cut -d "/" -f1 | rev`
		if [[ $dialstate != $bgLMtag ]]; then
			echo "Creating Interpolated LM for dialog state $dialstate ..."
			mixlmdir=$dsIntLMdir/$dialstate/mixlm/
			mkdir -p $mixlmdir
			bash local/dsLMs/interpolate_lms.sh $baseLM $d/srilm/lm.gz $d/srilm/dev.txt $mixlmdir > $mixlmdir/../mixlm-train.log 2>&1 || exit 1 
			bash utils/build_const_arpa_lm.sh $mixlmdir/lm.gz $langdir $mixlmdir/../carpa_lm > $mixlmdir/../carpa-build.log 2>&1 || exit 1
		else
			echo "Skipping dialog state $dialstate ..."
			cp -R $d $dsIntLMdir
		fi
	fi
done

