#!/usr/bin/env perl

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV!=2){
        print "\nUSAGE: $0 <saus.txt> <error.preds> <unkid>\n\n";
        exit(1);
}


$filename = $ARGV[1];
my %errPreds;
open(my $fhErrPreds, '<:encoding(UTF-8)', $filename)  or die "Could not open file '$filename' $!";
while (my $line = <$fhErrPreds>) {
    chomp $line;
    my ($fid, $preds) = split(/\s*\,\s*/, $line);
    $errPreds{$fid} = $preds;
}
close($fhErrPreds);

$filename = $ARGV[0];
open(my $fhSaus, '<:encoding(UTF-8)', $filename)  or die "Could not open file '$filename' $!";
while (my $line = <$fhSaus>) {
    chomp $line;

    my ($fid, $sausTxt) = split(/\s+/, $line, 2);

    if($fid =~ m/^sp/){
        next;
    }

    $sausTxt =~ s/^\s*\[\s*//;
    $sausTxt =~ s/\s*\]\s*$//;
    my @sausBins = split(/ \] \[ /, $sausTxt);
    my @sausBinsWoEpsOnlyArcs;
    for my $bin (@sausBins){
        if($bin !~ m/^0 1$/){
            push(@sausBinsWoEpsOnlyArcs, $bin);
        }
    }
    
    my @predErrArr = split(/\s+/, $errPreds{$fid});
    if($#predErrArr != $#sausBinsWoEpsOnlyArcs){
        print STDERR "Error: # of sausage bins and error predictions do not match!\n       $#predErrArr != $#sausBinsWoEpsOnlyArcs \n";
        print STDERR "$fid\n";
        exit(1);
    }

    my $i=0;
    my $outtxt =  $fid." ";
    foreach my $bin (@sausBinsWoEpsOnlyArcs){
        if($predErrArr[$i] == 1){      # 1=no-error, 2=no-error-eps, 3=error # mark arcs from no-error region in lattice
            my @binArcs = $bin =~ /((?:\S*\s?){1,2})/g;
            pop @binArcs;  # remove the extra element from begin

            my $wid = $binArcs[0];
            $wid =~ s/\s.*$//;
            $outtxt = $outtxt.$wid." ";
        }elsif($predErrArr[$i] == 2){
            $outtxt = $outtxt;            
        }else{
            $outtxt = $outtxt."$ARGV[2] "; # map errors to <UNK>; $ARGV[2] is word id of <UNK> in words.txt
        }

        $i = $i + 1;
    }
    $outtxt =~ s/\s*$//;
    print "$outtxt\n";
}
