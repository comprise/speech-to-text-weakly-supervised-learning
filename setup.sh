#!/bin/bash
#
# Copyright © 2021 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# This script with download and install Kaldi and kaldi_lm.
# Note: You can ignore this script if you already have these tools,
#       but remember to modify paths in path.sh and that of utils, steps. 
#
# Caution: It installs an older version of Kaldi !!!
#          This script is not tested in Windows and/or Cygwin OS !!!
#

install_kaldi=true    # set this to false if you have Kaldi installed
install_kaldi_lm=true # set this to false if you have kaldi_lm in the tools dir of your kaldi

echo ""
echo "$0 $@"  # Print the command line for logging
echo ""

kaldi_archive=https://github.com/kaldi-asr/kaldi/archive/
kaldi_version=1be47502c2aad58c4eb1cac987f0b0f0e515ef15
kaldi_version_short=`echo ${kaldi_version} | cut -c 1-7`
kaldi_zip=${kaldi_archive}/${kaldi_version}.zip

num_proc=4 # increasing this will speedup the setup if you have more CPUs

if [ $# != 1 ]; then
	echo ""
	echo "USAGE: $0 kaldi_install_dir"
	echo ""	
	exit 1;
fi

kaldi_install_dir=$1

for f in setup.sh path.sh; do
  [ ! -f $f ] && echo "$0: $f not found. Ensure you are in the same dir as setup.sh !" && exit 1;
done

mkdir -p ${kaldi_install_dir}
comprise_install_dir=`pwd`

# Install Kaldi and kaldi_lm
cd ${kaldi_install_dir}
if ${install_kaldi} ; then
	wget ${kaldi_zip} || exit 1
	unzip ${kaldi_version}.zip && mv kaldi-${kaldi_version} kaldi-${kaldi_version_short}
	rm ${kaldi_version}.zip 
	cd kaldi-${kaldi_version_short}
	kald_root=`pwd`
	cd tools
	./extras/check_dependencies.sh || exit 1
	make -j ${num_proc} || exit 1
	cd ../src 
	./configure --shared && make depend -j ${num_proc} && make -j ${num_proc} || exit 1
	cd ..
else
	kald_root=`pwd`
fi

if ${install_kaldi_lm} ; then
	cd tools/
	./extras/install_kaldi_lm.sh || exit 1
fi

# setup COMPRISE Weakly Supervised STT library
cd ${comprise_install_dir}
rm steps utils
ln -s ${kald_root}/egs/wsj/s5/steps .
ln -s ${kald_root}/egs/wsj/s5/utils .
cp local/kaldi-helpers/lattice-mbr-decode-wtimes.cc ${kald_root}/src/latbin/
cd ${kald_root}/src/latbin/
mv Makefile Makefile.orig
cat Makefile.orig | sed 's/lattice-mbr-decode /lattice-mbr-decode lattice-mbr-decode-wtimes /' > Makefile
make
echo "export KALDI_ROOT=${kald_root}" > path_new.sh
echo "export KALDI_LM_ROOT=${kald_root}/tools/kaldi_lm/" >> path_new.sh
tail -n +3 path.sh >> path_new.sh
mv path_new.sh path.sh
echo "----------------------------------------------------------------------"
echo "Note: Please update the path to your SRILM and SoX in path.sh file !!!"
echo "----------------------------------------------------------------------"
cd ${comprise_install_dir}