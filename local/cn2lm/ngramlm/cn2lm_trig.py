#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import time, os, re, sys
import numpy
from tqdm import tqdm
from poisson_binomial import PB, PK_UPPER_LIMIT

NO_DP = 1234.56789
NO_E_CNT = NO_DP
SKIP_SYM_LIST = ['<eps>', '#0']
UG_SKIP_SYM_LIST = ['<eps>', '#0', '<s>']   # skip in unigram prob calc

class CN2LM:
    def __init__(self, data):
        self.data = data
        self.vocab_dict = data.vocab_dict
        self.vocab_list = data.vocab_list
        self.N = len(self.vocab_dict)

        ### step 1 of 3.2 of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        ### step 1 of 4.6 of https://www.aclweb.org/anthology/P14-1072.pdf
        print('Computing 3g pks ...')
        self.tg_pk, self.bos_bg_pk = self.compute_tg_pks(data)  # computed from scores of 3g paths in sausages
        print('Computing 3g PB distributions ...')
        self.tg_pb = self.compute_pb_distributions()      # poisson-binomial count distribution

        print('Computing 3g expected counts ...')
        ### E(cw) in step 2 of 3.2 of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        self.tg_E_cnt = self.compute_tg_E_cnt()       # considering it as highest n-gram order      

        ### E_nr (r>4) and E_nr+ reqd for step 1 of 4.6 of https://www.aclweb.org/anthology/P14-1072.pdf
        ### other probs reqd in step 2 of 3.2 of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        # to rely on following function calls on ug_pb bg_pb     

        ### step 3 of 4.6 of https://www.aclweb.org/anthology/P14-1072.pdf , Note: lower order is unigram in our case
        ### step 3 of 3.2 of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/, Note lower order is unigram in our case
        print('Computing 2g expected counts and PB distributions...')
        self.bg_pb, self.bg_E_cnt = self.compute_bg_E_cnt()
        print('Computing 1g expected counts and PB distributions...')
        self.ug_pb, self.ug_E_cnt = self.compute_ug_E_cnt()
        
        print('Computing discount parameters...')
        ug_E_nr = numpy.zeros(5)                # E[n_r], where r in (0, 1, 2, 3, 4)
        bg_E_nr = numpy.zeros(5)
        tg_E_nr = numpy.zeros(5)
        for r in range(5):
            ug_E_nr[r] = self.compute_ug_E_nr(r)
            bg_E_nr[r] = self.compute_bg_E_nr(r)
            tg_E_nr[r] = self.compute_tg_E_nr(r)
    
        ### step 2 of 4.6 of https://www.aclweb.org/anthology/P14-1072.pdf
        ### step 4 of 3.2 of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/
        self.tg_D1, self.tg_D2, self.tg_D3 = self.compute_mod_kn_discounts(tg_E_nr)  
        self.bg_D1, self.bg_D2, self.bg_D3 = self.compute_mod_kn_discounts(bg_E_nr)  
        self.ug_D1, self.ug_D2, self.ug_D3 = self.compute_mod_kn_discounts(ug_E_nr)
        self.tg_DP = self.compute_mod_kn_avg_discount(self.tg_pb, self.tg_D1, self.tg_D2, self.tg_D3)
        self.bg_DP = self.compute_mod_kn_avg_discount(self.bg_pb, self.bg_D1, self.bg_D2, self.bg_D3)
        self.ug_DP = self.compute_mod_kn_avg_discount(self.ug_pb, self.ug_D1, self.ug_D2, self.ug_D3)

        print('Computing ngram probs and bows...')
        ### step 5  3.2 of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        self.ug_prob = self.compute_ug_probs()
        self.disc_bg_E_cnt = self.compute_disc_bg_E_cnt()
        self.ug_bow, self.bg_E_cnt_sum_for_prev_word = self.compute_ug_bow()
        self.bg_prob = self.compute_bg_prob()
        self.disc_tg_E_cnt = self.compute_disc_tg_E_cnt()
        self.bg_bow, self.tg_E_cnt_sum_for_prev_bg = self.compute_bg_bow()
        self.tg_prob = self.compute_tg_prob()

    def compute_tg_pks(self, data):
        tg_pk = {} # score of kth occurence of trigrams
        bos_bg_pk = {}

        bi = 0
        for batch in tqdm(data.yield_trig_batch(),total=data.total_trig_batches):
            start_time = time.time()
            for binngram in batch:
                t=0
                for h, h_arc_scr in binngram[t]:
                    for i, i_arc_scr in binngram[t+1]:
                        if h == self.vocab_dict['<s>']:
                            bg_scr = h_arc_scr * i_arc_scr
                            key = i
                            try:
                                bos_bg_pk[key].append(bg_scr)
                            except:
                                bos_bg_pk[key] = []
                                bos_bg_pk[key].append(bg_scr)
                        for j, j_arc_scr in binngram[t+2]:
                            tg_scr = h_arc_scr * i_arc_scr * j_arc_scr
                            key = "{}-{}-{}".format(h,i,j)
                            try:
                                tg_pk[key].append(tg_scr)
                            except:
                                tg_pk[key] = []
                                tg_pk[key].append(tg_scr)
            bi += 1
            #print("%d\t%.4f" % (bi, time.time() - start_time))
        return tg_pk, bos_bg_pk

    def compute_pb_distributions(self):
        tg_pb = {}        # poisson-binomial count distribution for trigrams
        for k,v in tqdm(self.tg_pk.items()):
            tg_pb[k] = PB(v)
        return tg_pb

    def compute_tg_E_nr(self, r): # as per (26) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        E_nr = 0.0
        for key in self.tg_pb.keys():
            pb = self.tg_pb[key]
            E_nr += self.pb_prob_cnt_eq_r(pb,r) # as per 26 E[n_r] = \sum_{all_n-gram} p(c(n-gram) = r), for highest order
        return E_nr

    def compute_bg_E_nr(self, r): # implied from (30) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf 
        E_nr = 0.0
        for key in self.bg_pb.keys():
            pb = self.bg_pb[key]
            E_nr += self.pb_prob_cnt_eq_r(pb,r)    #E[n_r(w_j)] = E[n_r(* w_j)] = \sum_{i} p(c(w_i w_j) = r)
        return E_nr

    def compute_ug_E_nr(self, r): # implied from (30) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf 
        E_nr = 0.0
        for key in self.ug_pb.keys():
            pb = self.ug_pb[key]
            E_nr += self.pb_prob_cnt_eq_r(pb,r)    #E[n_r(w_j)] = E[n_r(* w_j)] = \sum_{i} p(c(w_i w_j) = r)
        return E_nr

    def compute_tg_E_cnt(self):     # considering it as highest n-gram order, see (23) and (38) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        tg_E_cnt = {}
        for k,v in tqdm(self.tg_pk.items()):
            tg_E_cnt[k] = sum(v)
        return tg_E_cnt

    def compute_bg_E_cnt(self):     # from (35) and (38) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        bg_E_cnt = {}
        bg_pks = {}
        bg_pb = {}
        for key in self.tg_pb.keys():
            _h, bg_key = key.split('-',1)
            pb = self.tg_pb[key]
            pk = self.pb_prob_cnt_ge_r(pb,1)   # p(c(w_h w_i w_j) > 0) = p(c(w_h w_i w_j) >= 1)
            if pk <= 0.0:
                print("pb_prob_cnt_ge_1 is {} for {} and tg_pks {} {}".format(pk, key, self.tg_pk[key], self.tg_pb[key].pks))
            elif pk > 1.0:
                print('Warning: pk (' + str(pk) + ') > 1.0, setting to 0.99, for ' + self.vocab_list[int(_h)] + ' ' + self.vocab_list[int(bg_key.split('-')[0])] + ' ' + self.vocab_list[int(bg_key.split('-')[1])])
                pk = PK_UPPER_LIMIT
            if bg_key not in bg_pks.keys():
                bg_pks[bg_key] = []
            bg_pks[bg_key].append(pk)
        #for bg_key in bg_pks:
        for k,v in tqdm(bg_pks.items()):
            bg_pb[k] = PB(v)
            bg_E_cnt[k] = sum(v)
        for i in self.bos_bg_pk.keys():
            bg_key = "{}-{}".format(self.vocab_dict['<s>'],i)
            bg_E_cnt[bg_key] = sum(self.bos_bg_pk[i])
            bg_pb[bg_key] = PB(self.bos_bg_pk[i])
        return bg_pb, bg_E_cnt

    def compute_ug_E_cnt(self):     # from (35) and (38) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        ug_E_cnt = {}
        ug_pks = {}
        ug_pb = {}
        for key in self.bg_pb.keys():
            _i, ug_key = key.rsplit('-')
            if self.vocab_list[int(ug_key)] in UG_SKIP_SYM_LIST:
                ug_E_cnt[ug_key] = NO_E_CNT
            else:
                pb = self.bg_pb[key]
                pk = self.pb_prob_cnt_ge_r(pb,1)   # p(c(w_i w_j) > 0) = p(c(w_i w_j) >= 1)
                if pk > 1.0:
                    print('Warning: pk (' + str(pk) + ') > 1.0, setting to 0.99, for ' + self.vocab_list[int(_i)] + ' ' + self.vocab_list[int(ug_key)])
                    pk = PK_UPPER_LIMIT
                if ug_key not in ug_pks.keys():
                    ug_pks[ug_key] = []    
                ug_pks[ug_key].append(pk)
        for ug_key in ug_pks:
            if self.vocab_list[int(ug_key)] not in UG_SKIP_SYM_LIST:
                ug_E_cnt[ug_key] = sum(ug_pks[ug_key])
                ug_pb[ug_key] = PB(ug_pks[ug_key])
        return ug_pb, ug_E_cnt

    def compute_mod_kn_discounts(self, E_nr):   # (28) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        print(E_nr)
        Y = E_nr[1] / (E_nr[1] + 2 * E_nr[2])
        D1 = 1 - 2 * Y * E_nr[2] / E_nr[1]
        D2 = 2 - 3 * Y * E_nr[3] / E_nr[2]
        D3 = 3 - 4 * Y * E_nr[4] / E_nr[3]
        print("Discounts %f %f %f" % (D1, D2, D3))
        return D1, D2, D3

    def compute_mod_kn_avg_discount(self, pb_dict, D1, D2, D3): # (20) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        DP = {}
        for key,pb in tqdm(pb_dict.items()):
            p_cnt_eq_1 = self.pb_prob_cnt_eq_r(pb,1)
            p_cnt_eq_2 = self.pb_prob_cnt_eq_r(pb,2)
            pb_cnt_ge_3 = self.pb_prob_cnt_ge_r(pb,3)
            DP[key] = p_cnt_eq_1 * D1 + p_cnt_eq_2 * D2 + pb_cnt_ge_3 * D3
        return DP

    def compute_ug_probs(self):     # as per (49) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        ug_probs = {}
        disc_ug_probs = {}  # unigram discounted probs
        ug_E_cnt_sum = 0.0
        redistribute_cnt = 0
        for int_key in range(self.N):
            key = str(int_key)
            if self.vocab_list[int_key] in UG_SKIP_SYM_LIST:
                disc_ug_probs[key] = 0.0    # no prob, noredistribute_cnt
            elif key not in self.ug_E_cnt.keys():
                disc_ug_probs[key] = 0.0
                redistribute_cnt += 1
            elif self.ug_E_cnt[key] < 0.0:  # double check
                print('Fatal Error: ug_E_cnt < 0 for ' + self.vocab_list[int_key])
                disc_ug_probs[key] = 0.0
                redistribute_cnt += 1
            elif self.ug_E_cnt[key] == NO_E_CNT:
                disc_ug_probs[key] = 0.0
                redistribute_cnt += 1
            else:
                ug_E_cnt_sum += self.ug_E_cnt[key]
                disc_ug_probs[key] = (self.ug_E_cnt[key] - self.ug_DP[key])   # only numerator for discounted probs
                redistribute_cnt += 1
        
        psum = 0.0
        for k in disc_ug_probs.keys():
            disc_ug_probs[k] = disc_ug_probs[k] / ug_E_cnt_sum           # coversion to discounted probs
            psum += disc_ug_probs[k]
        left_over_prob_mass = 1.0 - psum
        redistribute_prob = left_over_prob_mass / redistribute_cnt       # redistribute uniformly to all
        
        for key in disc_ug_probs.keys():
            int_key = int(key)
            if self.vocab_list[int_key] == '<s>':
                ug_probs[self.vocab_list[int_key]] = 0.0
            elif self.vocab_list[int_key] not in UG_SKIP_SYM_LIST:
                ug_probs[self.vocab_list[int_key]] = disc_ug_probs[key] + redistribute_prob
        return ug_probs
        
    def compute_ug_bow(self):   # as per (55) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        ug_bow = {}
        bg_E_cnt_sum_for_prev_word = {}
        used_mass_for_prev_word = {}
        for key in self.bg_E_cnt.keys():
            prev_w_key,j=key.split('-')
            j=int(j)
            i=int(prev_w_key)
            if not(self.vocab_list[i] in (SKIP_SYM_LIST + ['</s>'])):
                if prev_w_key not in bg_E_cnt_sum_for_prev_word.keys():
                    bg_E_cnt_sum_for_prev_word[prev_w_key] = 0
                    used_mass_for_prev_word[prev_w_key] = 0
                bg_E_cnt_sum_for_prev_word[prev_w_key] += self.bg_E_cnt[key]
                used_mass_for_prev_word[prev_w_key] += self.disc_bg_E_cnt[key]
        
        for prev_w_key in bg_E_cnt_sum_for_prev_word:
            i=int(prev_w_key)
            if used_mass_for_prev_word[prev_w_key] >= 0.0 and bg_E_cnt_sum_for_prev_word[prev_w_key] > 0.0:
                if used_mass_for_prev_word[prev_w_key] > bg_E_cnt_sum_for_prev_word[prev_w_key]:
                    print("used_mass %f > bg_E_cnt_sum_for_prev_word %f for %s" %(used_mass_for_prev_word[prev_w_key], bg_E_cnt_sum_for_prev_word[prev_w_key], self.vocab_list[i]))
                else:
                    ug_bow[self.vocab_list[i]] = 1 - (used_mass_for_prev_word[prev_w_key] / bg_E_cnt_sum_for_prev_word[prev_w_key])
            else:
                print("Problem with used_mass %f or bg_E_cnt_sum_for_prev_word %f for %s" %(used_mass_for_prev_word[prev_w_key], bg_E_cnt_sum_for_prev_word[prev_w_key], self.vocab_list[i]))
        return ug_bow, bg_E_cnt_sum_for_prev_word

    def compute_bg_bow(self):   # as per (55) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        bg_bow = {}
        tg_E_cnt_sum_for_prev_bg = {}
        used_mass_for_prev_bg = {}
        for key in self.tg_E_cnt.keys():
            prev_bg_key=key.rsplit('-',1)[0]
            h,i,j=key.split('-')
            h=int(h)
            i=int(i)
            j=int(j)
            if not(self.vocab_list[h] in (SKIP_SYM_LIST + ['</s>'])):
                if self.tg_E_cnt[key] != NO_E_CNT and self.disc_tg_E_cnt[key] != NO_E_CNT:
                    if prev_bg_key not in tg_E_cnt_sum_for_prev_bg.keys():
                        tg_E_cnt_sum_for_prev_bg[prev_bg_key] = 0
                        used_mass_for_prev_bg[prev_bg_key] = 0
                    tg_E_cnt_sum_for_prev_bg[prev_bg_key] += self.tg_E_cnt[key]
                    used_mass_for_prev_bg[prev_bg_key] += self.disc_tg_E_cnt[key]
        for prev_bg_key in tg_E_cnt_sum_for_prev_bg:
            h,i=prev_bg_key.split('-')
            h=int(h)
            i=int(i)
            wh=self.vocab_list[h]
            wi=self.vocab_list[i]
            if used_mass_for_prev_bg[prev_bg_key] >= 0.0 and tg_E_cnt_sum_for_prev_bg[prev_bg_key] > 0.0:
                if used_mass_for_prev_bg[prev_bg_key] > tg_E_cnt_sum_for_prev_bg[prev_bg_key]:
                    print("used_mass %f > tg_E_cnt_sum_for_prev_word %f for %s" %(used_mass_for_prev_bg[prev_bg_key], tg_E_cnt_sum_for_prev_bg[prev_bg_key], self.vocab_list[h]))
                else:
                    if wh not in bg_bow.keys():
                        bg_bow[wh] = {}
                    bg_bow[wh][wi] = 1 - (used_mass_for_prev_bg[prev_bg_key] / tg_E_cnt_sum_for_prev_bg[prev_bg_key])
            else:
                print("Problem with used_mass %f or tg_E_cnt_sum_for_prev_bg %f for %s" %(used_mass_for_prev_bg[prev_bg_key], tg_E_cnt_sum_for_prev_bg[prev_bg_key], self.vocab_list[h]))
        return bg_bow, tg_E_cnt_sum_for_prev_bg

    def compute_bg_prob(self):    # as per (54) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        bg_probs = {}
        for key in self.disc_bg_E_cnt.keys():
            prev_w_key,j=key.split('-')
            i=int(prev_w_key)
            j=int(j)
            if not(self.vocab_list[i] in (SKIP_SYM_LIST + ['</s>'])): 
                if self.bg_E_cnt_sum_for_prev_word[prev_w_key] > 0.0:
                    if self.disc_bg_E_cnt[key] != NO_E_CNT:
                        wi = self.vocab_list[i]
                        wj = self.vocab_list[j]
                        if wi not in bg_probs.keys():
                            bg_probs[wi] = {}
                        bg_probs[wi][wj] = (self.disc_bg_E_cnt[key] / self.bg_E_cnt_sum_for_prev_word[prev_w_key]) + (self.ug_bow[wi] * self.ug_prob[wj])
                else:
                    print("Problem with bg_E_cnt_sum_for_prev_word %f for %s" %(self.bg_E_cnt_sum_for_prev_word[prev_w_key], self.vocab_list[i]))
        return bg_probs

    def compute_tg_prob(self):    # as per (54) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        tg_probs = {}
        for key in self.tg_E_cnt.keys():
            prev_bg_key=key.rsplit('-',1)[0]
            h,i,j=key.split('-')
            h=int(h)
            i=int(i)
            j=int(j)
            if not(self.vocab_list[h] in (SKIP_SYM_LIST + ['</s>'])): 
                if self.tg_E_cnt_sum_for_prev_bg[prev_bg_key] > 0.0:
                    if self.disc_tg_E_cnt[key] != NO_E_CNT:
                        wh = self.vocab_list[h]
                        wi = self.vocab_list[i]
                        wj = self.vocab_list[j]
                        if prev_bg_key not in tg_probs.keys():
                            tg_probs[prev_bg_key] = {}
                        if (wi in self.bg_prob.keys()) and (wj in self.bg_prob[wi].keys()):
                            tg_probs[prev_bg_key][wj] = (self.disc_tg_E_cnt[key] / self.tg_E_cnt_sum_for_prev_bg[prev_bg_key]) + (self.bg_bow[wh][wi] * self.bg_prob[wi][wj])
                        else:
                            tg_probs[prev_bg_key][wj] = (self.disc_tg_E_cnt[key] / self.tg_E_cnt_sum_for_prev_bg[prev_bg_key]) + (self.bg_bow[wh][wi] * self.ug_bow[wi] * self.ug_prob[wj])
                else:
                    print("Problem with tg_E_cnt_sum_for_prev_bg %f for %s %s" %(self.tg_E_cnt_sum_for_prev_bg[prev_bg_key], wh, wi))
        return tg_probs
        
    def compute_disc_bg_E_cnt(self):
        disc_bg_E_cnt = {}
        for key in self.bg_E_cnt.keys():
            i,j=key.split('-')
            i=int(i)
            j=int(j)
            if self.bg_E_cnt[key] != NO_E_CNT:
                disc_bg_E_cnt[key] = self.bg_E_cnt[key] - self.bg_DP[key]
                if self.bg_DP[key] > self.bg_E_cnt[key]:
                    print("-ve disc_bg_E_cnt[key] %f - %f  = %f for %s %s" %(self.bg_E_cnt[key], self.bg_DP[key], disc_bg_E_cnt[key], self.vocab_list[i], self.vocab_list[j]))
                    disc_bg_E_cnt[key] = 0.0
            else:
                disc_bg_E_cnt[key] = NO_E_CNT
        return disc_bg_E_cnt

    def compute_disc_tg_E_cnt(self):
        disc_tg_E_cnt = {}
        for key in self.tg_E_cnt.keys():
            h,i,j=key.split('-')
            h=int(h)
            i=int(i)
            j=int(j)
            if self.tg_E_cnt[key] != NO_E_CNT:
                disc_tg_E_cnt[key] = self.tg_E_cnt[key] - self.tg_DP[key]
                if self.tg_DP[key] > self.tg_E_cnt[key]:
                    print("-ve disc_tg_E_cnt[key] %f - %f = %f for %s %s %s" %(self.tg_E_cnt[key], self.tg_DP[key], disc_tg_E_cnt[key], self.vocab_list[h], self.vocab_list[i], self.vocab_list[j]))
                    disc_tg_E_cnt[key] = 0.0
            else:
                disc_tg_E_cnt[key] = NO_E_CNT
        return disc_tg_E_cnt

    def pb_prob_cnt_eq_r(self, pb, r):      # (24) (25) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        if pb.number_trials == 0 and r == 0:
            return 1.0
        elif r>=0 and r<=pb.number_trials:
            return pb.pmf(r)
        else:
            return 0.0

    def pb_prob_cnt_ge_r(self, pb, r):      # (31) of https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ExpectedKneserNey-Tech-Report.pdf
        _pb_prob_cnt_eq_r = self.pb_prob_cnt_eq_r(pb,r) 
        sum_prob_cnt_lt_r = 0.0
        _r = 0
        while _r < r:
            sum_prob_cnt_lt_r += self.pb_prob_cnt_eq_r(pb,_r)
            _r += 1
        _pb_prob_cnt_ge_r = 1 - sum_prob_cnt_lt_r
        return max([_pb_prob_cnt_eq_r, _pb_prob_cnt_ge_r])