#!/bin/bash

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

echo ""
echo "$0 $@"  # Print the command line for logging
echo ""

[ -f ./path.sh ] && . ./path.sh

if [ $# != 4 ]; then
	echo ""
	echo "USAGE: $0 asr_vocab_file kaldi_gru_lm_template_file pytorch_model out_kaldi_model_dir"
	echo ""	
	exit 1;
fi

vocab=$1
rawtmp=$2
torchmdl=$3
dir=$4

for f in $vocab $rawtmp $torchmdl; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

outdir=${dir}/kaldi-format
mkdir -p $outdir/config

python -u local/cn2lm/rnnlm/kaldi_support/pytorch_grulm_to_kaldi.py $vocab $rawtmp $torchmdl $outdir || exit -1;

nnet3-copy $outdir/final $outdir/final.raw
copy-matrix $outdir/word_embedding.final $outdir/word_embedding.final.mat

brkid=`wc -l $vocab | cut -d" " -f1`
cp $vocab $outdir/config/words.txt
echo "<brk> $brkid" >> $outdir/config/words.txt

eosid=`echo $brkid-1 | bc`
bosid=`echo $brkid-2 | bc`
echo "--bos-symbol=$bosid --eos-symbol=$eosid --brk-symbol=$brkid" > $outdir/special_symbol_opts.txt

unksym=`grep -i "<UNK>" $vocab | cut -d" " -f1`
echo "$unksym" > $outdir/config/oov.txt

