export KALDI_ROOT="/srv/storage/talc@talc-data.nancy/multispeech/calcul/users/imsheikh/tools/kaldi-1be4750/"
export KALDI_LM_ROOT="/srv/storage/talc@talc-data.nancy/multispeech/calcul/users/imsheikh/tools/kaldi_lm"
export SRILM="/srv/storage/talc3@talc-data.nancy/multispeech/calcul/users/isheikh/tools/kaldi-a4b6388/tools/srilm"
export SOX_ROOT="/srv/storage/talc@talc-data.nancy/multispeech/calcul/users/imsheikh/tools/sox/sox-14.4.2_install/bin/"

export WD=`readlink -f .`

export PATH=$WD:$WD/utils:$KALDI_ROOT/src/bin:$KALDI_ROOT/tools/openfst/bin:$PATH
export PATH=$KALDI_LM_ROOT:$PATH
export PATH=${PATH}:${SRILM}/bin:${SRILM}/bin/i686-m64
export PATH=$SOX_ROOT:$PATH

export LD_LIBRARY_PATH=$KALDI_ROOT/tools/openfst/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$SOX_ROOT/../lib:$LD_LIBRARY_PATH

export LC_ALL=C

[ ! -f $KALDI_ROOT/tools/config/common_path.sh ] && echo >&2 "The standard file $KALDI_ROOT/tools/config/common_path.sh is no
t present -> Exit!" && exit 1
. $KALDI_ROOT/tools/config/common_path.sh