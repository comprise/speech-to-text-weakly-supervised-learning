#!/usr/bin/env perl

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 1){
	print STDERR "USAGE: $0 <hyp_trans_file> <orig_trans_csv>\n";
	exit(0);
}

my %dsMap;
open my $FH, "<", $ARGV[1] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($rawid, $txt, $ds) = split(/\,/, $line);
	$dsMap{$rawid} = $ds;
}
close($FH);

open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	$id =~ s/\_/\//g;
	$id = "$id.raw";
	print $id.",".$txt.",".$dsMap{$id}."\n";
}
close($FH);