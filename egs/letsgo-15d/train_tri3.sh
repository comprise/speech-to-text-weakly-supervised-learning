#!/bin/bash

# Kaldi recipe for traning GMM-HMM models on (a subset of) LetsGo corpus.
#
# Derived software, Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# Based on Kaldi, Copyright © 2019 Johns Hopkins University
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

if [ $# != 1 ]; then
    echo "USAGE: $0 letsgo_raw_directory"
    echo "          letsgo_raw_directory is the parent directory containing Let's Go raw data."
    exit
fi

wav_dir=$1 # absolute path to dir containing LetsGoDataset

datadir=data/letsgo-15d-seed/
expdir=exp/letsgo-15d-seed/
confdir=egs/letsgo-15d/conf/
localdir=egs/letsgo-15d/local/

lang_stage=true     # set to false if lang preparation step is already completed.
fe_stage=true       # set to false if mfcc feature extraction step is already completed.
split_stage=true    # set to false if mono/tri1/tri2 training spliting step is already completed.
mono_stage=true     # set to false if mono training step is already completed.
tri1_stage=true     # set to false if tri1 training step is already completed.
tri2_stage=true     # set to false if tri2 training step is already completed.
tri3_stage=true      # tri3 on partial trainset; set to false if this training step is already completed.
tri3_full_stage=true # tri3 on full trainset; set to false if this training step is already completed.

feats_nj=40	     # num of jobs for feat extraction
train_nj=40	     # num of jobs for train
decode_nj=20	     # num of jobs for decode

# Acoustic model parameters (Note: u may have to vary this based on size of your datasets !!!)
numLeavesTri1=2000
numGaussTri1=10000
numLeavesTri2=2500
numGaussTri2=15000
numLeavesTri3=2500
numGaussTri3=15000
numLeavesTri3Full=3500
numGaussTri3Full=25000

# number of utterances for different AM (Note: u may have to vary this based on size of your datasets !!!)
monoSize=500   # shortest
tri1Size=700   # random
tri2Size=1000   # random

trainSplit=train    # directory for storing/using the train split data files
devSplit=dev        # directory for storing/using the dev   split data files
testSplit=test      # directory for storing/using the test  split data files

. ./path.sh  # set paths to binaries
. ./cmd.sh   # This relates to the execution queue.

set -e

. utils/parse_options.sh  # e.g. this parses the options if supplied. (currently disabled.)

if $lang_stage; then
  echo ============================================================================
  echo "                Data & Lexicon & Language Preparation                     "
  echo ============================================================================
  ### following steps will require contents in 'data_dir/local/dict' and '$datadir/local/local_lm'
  utils/prepare_lang.sh $datadir/local/dict "<unk>" $datadir/local/lang_tmp $datadir/lang || exit 1;
  $localdir/train_lm.sh $datadir || exit 1;        # might repeat some steps from the scripts run offline but it helps to keep in sync with Kaldi steps
  $localdir/format_local_lm.sh --datadir $datadir || exit 1;
fi

if $fe_stage; then
  echo ============================================================================
  echo "         MFCC Feature Extration & CMVN for Training and Dev set          "
  echo ============================================================================
  for x in $trainSplit $devSplit $testSplit; do
    mkdir -p $datadir/split/$x
    perl $localdir/generateWavScp.pl $wav_dir $datadir/trans/$x.transcriptions.csv $datadir/split/$x/ || exit 1;

    sort $datadir/split/$x/wav.scp.unsorted > $datadir/split/$x/wav.scp
    sort $datadir/split/$x/utt2spk.unsorted > $datadir/split/$x/utt2spk
    sort $datadir/split/$x/spk2utt.unsorted > $datadir/split/$x/spk2utt
    sort $datadir/split/$x/text.unsorted > $datadir/split/$x/text
    #rm -f $datadir/split/$x/*.unsorted

    steps/make_mfcc.sh --cmd "$train_cmd" --mfcc-config $confdir/mfcc.conf --nj $feats_nj $datadir/split/$x || exit 1;
    steps/compute_cmvn_stats.sh $datadir/split/$x || exit 1;
  done
fi

if $split_stage; then
  echo ============================================================================
  echo "                Create Mono/Tri1/Tri2 splits for Training                 "
  echo ============================================================================
  ## make subset with shortest $monoSize utt (?hr data) to train mono models
  utils/subset_data_dir.sh --shortest $datadir/split/$trainSplit $monoSize $datadir/split/$trainSplit"_s"$monoSize || exit 1  # use 5k in a normal situation

  ## make subset with random $tri1Size utterances to train tri1 models.
  utils/subset_data_dir.sh $datadir/split/$trainSplit $tri1Size $datadir/split/$trainSplit"_r"$tri1Size || exit 1;  # use 5k in a normal situation

  ## make subset with random $tri2Size utterances to train tri2 models.
  utils/subset_data_dir.sh $datadir/split/$trainSplit $tri2Size $datadir/split/$trainSplit"_r"$tri2Size || exit 1;  # use 10k in a normal situation
fi

if $mono_stage; then
  echo ============================================================================
  echo "                     MonoPhone Training & Decoding                        "
  echo ============================================================================
  steps/train_mono.sh --nj "$train_nj" --cmd "$train_cmd" $datadir/split/$trainSplit"_s"$monoSize $datadir/lang $expdir/mono

  # Commented to avoid long waits, an alternative is to run in background if you have computing resources
  #utils/mkgraph.sh $datadir/lang_test_tg $expdir/mono $expdir/mono/graph
  #steps/decode.sh --nj "$decode_nj" --cmd "$decode_cmd" $expdir/mono/graph $datadir/split/$devSplit $expdir/mono/decode_dev 
fi

if $tri1_stage; then
  echo ============================================================================
  echo "           tri1 : Deltas + Delta-Deltas Training & Decoding               "
  echo ============================================================================
  # Align te-in-Train_r5k data with mono models.
  steps/align_si.sh --boost-silence 1.25 --nj "$train_nj" --cmd "$train_cmd" $datadir/split/$trainSplit"_r"$tri1Size $datadir/lang $expdir/mono $expdir/mono_ali

  # From monophone model, train tri1 which is Deltas + Delta-Deltas.
  steps/train_deltas.sh --cmd "$train_cmd" $numLeavesTri1 $numGaussTri1 $datadir/split/$trainSplit"_r"$tri1Size $datadir/lang $expdir/mono_ali $expdir/tri1

  # Commented to avoid long waits, an alternative is to run in background if you have computing resources
  #utils/mkgraph.sh $datadir/lang_test_tg $expdir/tri1 $expdir/tri1/graph
  #steps/decode.sh --nj "$decode_nj" --cmd "$decode_cmd" $expdir/tri1/graph $datadir/split/$devSplit $expdir/tri1/decode_dev
fi 

if $tri2_stage; then
  echo ============================================================================
  echo "                 tri2 : LDA + MLLT Training & Decoding                    "
  echo ============================================================================
  # Align te-in-Train_r10k data with tri1 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri1 $expdir/tri1_ali

  # From tri1 system, train tri2 which is LDA + MLLT.
  steps/train_lda_mllt.sh --cmd "$train_cmd" --splice-opts "--left-context=3 --right-context=3" $numLeavesTri2 $numGaussTri2 $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri1_ali $expdir/tri2

  # Commented to avoid long waits, an alternative is to run in background if you have computing resources
  #utils/mkgraph.sh $datadir/lang_test_tg $expdir/tri2 $expdir/tri2/graph
  #steps/decode.sh --nj "$decode_nj" --cmd "$decode_cmd" $expdir/tri2/graph $datadir/split/$devSplit $expdir/tri2/decode_dev
fi

if $tri3_stage; then
  echo ============================================================================
  echo "              tri3 : LDA + MLLT + SAT Training & Decoding                 "
  echo ============================================================================
  # Align te-in-Train_r10k data with tri2 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" --use-graphs true $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri2 $expdir/tri2_ali

  # From tri2 system, train tri3 which is LDA + MLLT + SAT.
  steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3 $numGaussTri3 $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri2_ali $expdir/tri3

  # Commented to avoid long waits, an alternative is to run in background if you have computing resources
  #utils/mkgraph.sh $datadir/lang_test_tg $expdir/tri3 $expdir/tri3/graph
  #steps/decode_fmllr.sh --nj "$decode_nj" --cmd "$decode_cmd" $expdir/tri3/graph $datadir/split/$devSplit $expdir/tri3/decode_dev
fi

if $tri3_full_stage; then
  echo ============================================================================
  echo "              tri3 full: LDA + MLLT + SAT Training & Decoding              "
  echo ============================================================================
  # Align full train data with tri3 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" --use-graphs true $datadir/split/$trainSplit $datadir/lang $expdir/tri3 $expdir/tri3_ali

  # From tri3 system, train tri3_full which is also LDA + MLLT + SAT.
  steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3Full $numGaussTri3Full $datadir/split/$trainSplit $datadir/lang $expdir/tri3_ali $expdir/tri3_full

  utils/mkgraph.sh $datadir/lang_test_tg $expdir/tri3_full $expdir/tri3_full/graph
  steps/decode_fmllr.sh --nj "$decode_nj" --cmd "$decode_cmd" $expdir/tri3_full/graph $datadir/split/$devSplit $expdir/tri3_full/decode_dev
fi

