if($#ARGV != 1){
	print STDERR "USAGE: $0 <lexicon.txt.orig> <oov.lexicon>\n";
	exit(0);
}

my %lookup;
open my $FH, "<", $ARGV[1] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	my ($wrd, $lex) = split(/\s+/, $line,2);
	$lookup{lc($wrd)} = lc($lex);
}
close($FH);

open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	if($line =~ m/add\_manually/){
		my ($wrd, $lex) = split(/\s+/, $line,2);
		print $wrd." ".$lookup{$wrd};
	}else{
		print $line;
	}
}
close($FH);
