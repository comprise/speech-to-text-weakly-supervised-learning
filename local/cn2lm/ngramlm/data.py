#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.

import os, re, sys

ONLY_PROB = 1
EQUI_PROB = False
SKIP_EPS = True
SKIP_NON_BEST_EPS = True
MAX_ARCS = 5 # set to 1 for BEST_SEQ
NGRAM_BATCH_SIZE = 500

class DataLoader:
    def __init__(self, asr_vocab_file, sup_text=None, unsup_saus_dir=None, unsup_errpreds=None):
        assert not(sup_text is None and unsup_saus_dir is None), "At least one of sup_text or unsup_saus_dir is required !!!"
        self.asr_vocab_file = asr_vocab_file
        self.sup_text = sup_text
        self.unsup_saus_dir = unsup_saus_dir

        self.vocab_dict = {}
        with open(self.asr_vocab_file) as fp:
            for line in fp:
                line = line.rstrip()
                word, wid = line.split(' ')
                self.vocab_dict[word] = int(wid)
        self.vocab_list = [None] * len(self.vocab_dict)
        for w in self.vocab_dict.keys():
            wid = self.vocab_dict[w]
            self.vocab_list[wid] = w

        self.unk_token = '<unk>'
        if '<UNK>' in self.vocab_dict:
            self.unk_token = '<UNK>'

        self.obs_sequences = []
        if unsup_saus_dir is not None:
            unsup_obs_seq = self.read_saus_dir(unsup_saus_dir, unsup_errpreds)
            self.obs_sequences += unsup_obs_seq

        if sup_text is not None:
            sup_obs_seq = self.read_sup_text(sup_text)
            self.obs_sequences += sup_obs_seq            

        total_trig_batches = 0
        for seq in self.obs_sequences:
            total_trig_batches += len(seq)-2
        self.total_trig_batches = int(total_trig_batches/NGRAM_BATCH_SIZE + 1)

    def read_error_preds(self, errpreds_file):
        err_preds = {}
        with open(errpreds_file) as fp:
            for line in fp:
                uttid, preds = line.strip().split(', ')
                err_preds[uttid] = [int(p) for p in preds.split(' ')]
        return err_preds

    def read_saus_dir(self, saus_dir, unsup_errpreds):
        err_preds = None
        if unsup_errpreds is not None:
            err_preds = self.read_error_preds(unsup_errpreds)

        allObsSeq = []
        for r, d, f in os.walk(saus_dir):
            for file in f:
                m = re.search(r'saus\-wtimes\.\d+$', file)
                if m is not None:
                    _fnum = file.split('.')[-1]
                    with open(os.path.join(r, file)) as fp: 
                        for line in fp:
                            line = line.replace('\n', ' ')

                            uttid, binText = line.split(' ', 1)
                            binText = binText.lstrip(' [')
                            binText = binText.rstrip('] ')
                            tmp_bins = binText.split(' ] [ ')	
                            _bins = []
                            for bin in tmp_bins:
                                if bin != '0 1':
                                    _bins.append(bin)

                            if err_preds is not None:
                                if len(err_preds[uttid]) != len(_bins):
                                    print("Error predictions for %s do match in length ! %d ! %d" %(uttid, len(err_preds[uttid]), len(_bins)))
                                    sys.exit()

                            binseq = []
                            for binid in range(len(_bins)):
                                bin = _bins[binid]
                                vals = bin.split(' ')
                                vid = 0
                                bestWrd = self.vocab_list[int(vals[0])]
                                bestScr = float(vals[1])
                                ######## 
                                if err_preds is not None and err_preds[uttid][binid]==2: # <eps>
                                    continue
                                if bestWrd == '<eps>' and (bestScr > 0.85 or SKIP_EPS): # for both <error> and <no-error>
                                    continue
                                else:
                                    num_bin_arcs = 0
                                    binprobs = []
                                    no_break = True
                                    while (num_bin_arcs < MAX_ARCS) and (vid < len(vals)) and no_break:
                                        wid = int(vals[vid])
                                        wrd = self.vocab_list[wid]
                                        vid += 1
                                        scr = float(vals[vid])
                                        vid += 1
                                        num_bin_arcs += 1
                                        if EQUI_PROB or (MAX_ARCS==1):
                                            scr = ONLY_PROB
                                        if SKIP_NON_BEST_EPS:
                                            if (bestWrd != '<eps>' and wrd == '<eps>'):
                                                continue 
                                        if (err_preds is not None) and (err_preds[uttid][binid]==1): # <no-error>
                                            no_break = False
                                            scr = ONLY_PROB
                                        binprobs.append([wid, scr])
                                    assert len(binprobs) <= MAX_ARCS, "Fatal Error: len(binprobs) > MAX_ARCS ------ {}".format(binprobs)         
                                    binseq.append(binprobs)
                            aseq = [[[self.vocab_dict['<s>'], ONLY_PROB]]] + binseq + [[[self.vocab_dict['</s>'], ONLY_PROB]]]
                            if len(aseq)<=2:
                                #print(_uttid, aseq)
                                continue
                            allObsSeq.append(aseq)
        return allObsSeq

    def read_sup_text(self, sup_text):
        obsseq = []
        with open(sup_text) as fp:
            for line in fp:
                line = line.rstrip()
                if ' ' not in line:
                    print('Skipping ' + line)
                    continue
                _uttid, text = line.split(' ', 1)
                wrds = text.split()
                seq = []
                for token in wrds:
                    if token == '<heps>':
                        token = '<eps>'
                    if token != '<eps>':	# exclude <eps>
                        if token in self.vocab_dict.keys():
                            token_id = self.vocab_dict[token]
                        else:
                            # should be ignored as per srilm http://www.speech.sri.com/projects/srilm/manpages/srilm-faq.7.html but practically not so????
                            token_id = self.vocab_dict[self.unk_token]
                        seq.append([[token_id, ONLY_PROB]]) # bin with 1 arc
                aseq = [[[self.vocab_dict['<s>'], ONLY_PROB]]] + seq + [[[self.vocab_dict['</s>'], ONLY_PROB]]]
                if len(aseq)<=2:
                    continue
                obsseq.append(aseq)
        return obsseq

    def yield_trig_batch(self):
        obseq_batch = []
        cnt = 0
        for obsSeq in sorted(self.obs_sequences, key=len):
            for t in range(len(obsSeq)-2):
                obseq_batch.append(obsSeq[t:t+3])
                cnt += 1
                if cnt == NGRAM_BATCH_SIZE:
                    yield obseq_batch
                    obseq_batch = []
                    cnt = 0
        if obseq_batch:
            yield obseq_batch