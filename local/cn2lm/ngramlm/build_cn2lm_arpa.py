#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# AGPL-3.0-only or AGPL-3.0-or-later  (https://www.gnu.org/licenses/agpl-3.0.txt)

# This file is part of CN2LM.
#
# CN2LM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CN2LM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CN2LM.  If not, see <https://www.gnu.org/licenses/>.


import argparse, os, sys, numpy

from data import DataLoader
from cn2lm_trig import CN2LM

parser = argparse.ArgumentParser()
parser.add_argument('asr_vocab_file', type=str, help='Path to ASR words.txt file')
parser.add_argument('sup_text', type=str, help='Path to sup text transcriptions')
parser.add_argument('unsup_saus_dir', type=str, help='Path to unsup saus dir')
parser.add_argument('out_dir', type=str, help='output dir')
args = parser.parse_args()

if not os.path.isfile(args.asr_vocab_file):
	raise IOError(args.asr_vocab_file)

if not os.path.isfile(args.sup_text):
	raise IOError(args.sup_text)

if not os.path.isdir(args.unsup_saus_dir):
	raise IOError(args.unsup_saus_dir)

if not os.path.isdir(args.out_dir):
	raise IOError(args.out_dir)

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

data = DataLoader(args.asr_vocab_file, args.sup_text, args.unsup_saus_dir)
#data = DataLoader(args.asr_vocab_file, args.sup_text, None) # support to train only on labeled reference
#data = DataLoader(args.asr_vocab_file, None, args.unsup_saus_dir)	# support to train only on sausages
#data = DataLoader(args.asr_vocab_file, args.sup_text, args.unsup_saus_dir, args.unsup_errpreds) # support to incorporate error predictions
cnlm = CN2LM(data)

with open(args.out_dir + '/cn2lm_3g.arpa', 'w') as fp:
	fp.write("\\data\\\n")
	fp.write("ngram 1="+ str(len(cnlm.ug_prob)) + "\n")
	bg_cnt = 0
	for h in cnlm.bg_prob.keys():
		for w in cnlm.bg_prob[h].keys():
			p = cnlm.bg_prob[h][w]
			if p != 0.0:
				bg_cnt += 1
	fp.write("ngram 2=" + str(bg_cnt)+ "\n")
	tg_cnt = 0
	for prev_bg_key in cnlm.tg_prob.keys():
		for j in cnlm.tg_prob[prev_bg_key].keys():
			p = cnlm.tg_prob[prev_bg_key][j]
			if p != 0.0:
				tg_cnt += 1
	fp.write("ngram 3=" + str(tg_cnt)+ "\n")
	fp.write("\n\\1-grams:\n")
	for w in cnlm.ug_prob.keys():
		p = cnlm.ug_prob[w]
		if p == 0.0:
			log_p = -99
		else:
			log_p = numpy.log10(p)
		if w in cnlm.ug_bow.keys():
			log_bow = numpy.log10(cnlm.ug_bow[w])
			fp.write("%f\t%s\t%f\n" % (log_p, w, log_bow))
		else:
			fp.write("%f\t%s\n" % (log_p, w))
	fp.write("\n\\2-grams:\n")
	for h in cnlm.bg_prob.keys():
		for w in cnlm.bg_prob[h].keys():
			p = cnlm.bg_prob[h][w]
			if p == 0.0:
				log_p = -99
			else:
				log_p = numpy.log10(p)
			
			if h in cnlm.bg_bow and w in cnlm.bg_bow[h]:
				log_bow = numpy.log10(cnlm.bg_bow[h][w])
				fp.write("%f\t%s %s\t%f\n" % (log_p, h, w, log_bow))
			else:
				fp.write("%f\t%s %s\n" % (log_p, h, w))
	fp.write("\n\\3-grams:\n")
	for prev_bg_key in cnlm.tg_prob.keys():
		h,i=prev_bg_key.split('-')
		for j in cnlm.tg_prob[prev_bg_key].keys():
			p = cnlm.tg_prob[prev_bg_key][j]
			if p == 0.0:
				log_p = -99
			else:
				log_p = numpy.log10(p)
			fp.write("%f\t%s %s %s\n" % (log_p, cnlm.vocab_list[int(h)], cnlm.vocab_list[int(i)], j))
	fp.write("\n\\end\\\n")
